package com.expenshare.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.expenshare.app.dialogs.DialogFragmentShowImage;
import com.expenshare.app.utilities.SlidingTabLayout;

import java.util.Locale;

public class TutorialActivity extends AppCompatActivity {


    SectionsPagerAdapter mSectionsPagerAdapter;
    SlidingTabLayout mSlidingTabLayout;

    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.tutorial_section_basics).toUpperCase(l);
                case 1:
                    return getString(R.string.tutorial_section_budget_overview).toUpperCase(l);
                case 2:
                    return getString(R.string.tutorial_section_adding_expense).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            int sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
            View rootView;
            switch (sectionNumber) {
                case 1:
                    rootView = inflater.inflate(R.layout.fragment_tutorial_basics, container, false);
                    break;
                case 2:
                    rootView = inflater.inflate(R.layout.fragment_tutorial_budgets, container, false);
                    break;
                case 3:
                    rootView = inflater.inflate(R.layout.fragment_tutorial_add_expense, container, false);
                    break;
                default:
                    rootView = inflater.inflate(R.layout.fragment_tutorial_basics, container, false);

            }

            return rootView;
        }
    }

    public void showBasicsExpensesDetails(View view) {
        int imageResource = R.drawable.img_basics_expenses_details;
        DialogFragmentShowImage dialog = DialogFragmentShowImage.newInstance(imageResource);
        dialog.show(getFragmentManager(), "imageDialog");
    }

    public void showBasicsPersonsDetails(View view) {
        int imageResource = R.drawable.img_basics_persons_details;
        DialogFragmentShowImage dialog = DialogFragmentShowImage.newInstance(imageResource);
        dialog.show(getFragmentManager(), "imageDialog");
    }

    public void showBudgetExpensesDetails(View view) {
        int imageResource = R.drawable.img_budget_expense_details;
        DialogFragmentShowImage dialog = DialogFragmentShowImage.newInstance(imageResource);
        dialog.show(getFragmentManager(), "imageDialog");
    }

    public void showBudgetPersonsDetails(View view) {
        int imageResource = R.drawable.img_budget_person_details;
        DialogFragmentShowImage dialog = DialogFragmentShowImage.newInstance(imageResource);
        dialog.show(getFragmentManager(), "imageDialog");
    }

}
