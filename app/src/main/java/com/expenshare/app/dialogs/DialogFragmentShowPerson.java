package com.expenshare.app.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.adapters.PersonsExpensesAdapter;
import com.expenshare.app.businesslogic.Person;
import com.expenshare.app.businesslogic.PersonExpense;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dataacces.TablePersons;
import com.expenshare.app.loaders.PersonsExpensesLoader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;

/**
 * Created by Maciek on 2014-06-29.
 */
public class DialogFragmentShowPerson extends DialogFragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<ArrayList> {

    private Button mOkButton;
    private TextView mPersonNameTextView;
    private TextView mPersonTotalSpendingTextView;
    private TextView mPersonTotalPaymentTextView;
    private TextView mPersonSpendingCurrencyTextView;
    private TextView mPersonPaymentCurrencyTextView;

    private TextView mTotalPersonCommitmentTextView;
    private TextView mTotalPersonCommitmentCurrencyTextView;

    private ListView mListView;

    private Person mPerson;
    //Adapters
    private PersonsExpensesAdapter mListViewAdapter;

    //Data
    private static final String PERSON__ARG = "person";

    public DialogFragmentShowPerson() {
    }

    static public DialogFragmentShowPerson newInstance(Person person) {
        DialogFragmentShowPerson f = new DialogFragmentShowPerson();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PERSON__ARG, person);
        f.setArguments(bundle);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPerson = getArguments().getParcelable(PERSON__ARG);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_show_person, null);

        setupControls(view);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(433, null, this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.okButton:

                dismiss();
                break;
        }

    }


    private void setupControls(View view) {

        mPersonNameTextView = (TextView) view.findViewById(R.id.titleTextView);
        mPersonNameTextView.setText(mPerson.getName());

        mPersonTotalSpendingTextView = (TextView) view.findViewById(R.id.personSpendingTextView);
        mPersonTotalPaymentTextView = (TextView) view.findViewById(R.id.personPaymentTextView);
        mPersonSpendingCurrencyTextView = (TextView) view.findViewById(R.id.personSpendingCurrencyCodeTextView);
        mPersonPaymentCurrencyTextView = (TextView) view.findViewById(R.id.personPaymentCurrencyCodeTextView);
        mTotalPersonCommitmentCurrencyTextView= (TextView) view.findViewById(R.id.totalPersonCommitmentCurrencyTextView);
        mTotalPersonCommitmentTextView= (TextView) view.findViewById(R.id.totalPersonCommitmentTextView);
        mOkButton = (Button) view.findViewById(R.id.okButton);
        mOkButton.setOnClickListener(this);

        mListView = (ListView) view.findViewById(R.id.personsExpensesListView);

    }


    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {

        String[] selectionArgs=new String[] {String.valueOf(mPerson.getId())};
        return new PersonsExpensesLoader(getActivity(), MyContentProvider.CONTENT_URI_PERSONS_EXPENSES, null, TablePersons.COLUMN_ID+"=?", selectionArgs, null);


    }


    @Override
    public void onLoadFinished(final Loader<ArrayList> loader, final ArrayList data) {
        final ArrayList<PersonExpense> personExpenses = (ArrayList<PersonExpense>) data;
        mListView.post(new Runnable() {
            @Override
            public void run() {

                mListViewAdapter = new PersonsExpensesAdapter(getActivity(), R.layout.cell_list_view_persons_expense,  personExpenses);
                mListView.setAdapter(mListViewAdapter);

            }


        });

        if(personExpenses.size()>0){
            BigDecimal personTotalSpendingsPart=BigDecimal.ZERO;
            BigDecimal personTotalPaymentsPart=BigDecimal.ZERO;
            for(PersonExpense personExpense: personExpenses){
                personTotalSpendingsPart=personTotalSpendingsPart.add(personExpense.getPersonSpending());
                personTotalPaymentsPart=personTotalPaymentsPart.add(personExpense.getPersonPayment());
            }

            final BigDecimal personTotalSpendings=personTotalSpendingsPart;
            final BigDecimal personTotalPayments=personTotalPaymentsPart;
            mPersonTotalSpendingTextView.post(new Runnable() {
                @Override
                public void run() {

                    mPersonTotalSpendingTextView.setText(personTotalSpendings.toString());

                }
            });

            mPersonTotalPaymentTextView.post(new Runnable() {
                @Override
                public void run() {
                    mPersonTotalPaymentTextView.setText(personTotalPayments.toString());

                }
            });

            mPersonSpendingCurrencyTextView.post(new Runnable() {
                @Override
                public void run() {
                    mPersonSpendingCurrencyTextView.setText(Currency.getInstance(personExpenses.get(0).getExpense().getCurrencyCode()).getSymbol());

                }
            });


            mPersonPaymentCurrencyTextView.post(new Runnable() {
                @Override
                public void run() {
                    mPersonPaymentCurrencyTextView.setText(Currency.getInstance(personExpenses.get(0).getExpense().getCurrencyCode()).getSymbol());

                }
            });



            mTotalPersonCommitmentCurrencyTextView.post(new Runnable() {
                @Override
                public void run() {

                    mTotalPersonCommitmentCurrencyTextView.setText(Currency.getInstance(personExpenses.get(0).getExpense().getCurrencyCode()).getSymbol());


                }
            });

            mTotalPersonCommitmentTextView.post(new Runnable() {
                @Override
                public void run() {
                    BigDecimal totalDifference=personTotalPayments.subtract(personTotalSpendings);

                    if(totalDifference.signum()==-1)
                        mTotalPersonCommitmentTextView.setTextColor(getResources().getColor(R.color.red));
                    else
                        mTotalPersonCommitmentTextView.setTextColor(getResources().getColor(R.color.green));
                    mTotalPersonCommitmentTextView.setText(totalDifference.toString());
                }
            });


        }


    }

    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }
}

