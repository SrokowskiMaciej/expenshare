package com.expenshare.app.dialogs;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;

import android.app.Dialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.expenshare.app.R;
import com.expenshare.app.adapters.AddExpenseEqualBaseModeListViewAdapter;
import com.expenshare.app.adapters.AddExpenseListViewBaseAdapter;
import com.expenshare.app.adapters.AddExpensePersonsSpinnerAdapter;
import com.expenshare.app.adapters.AddExpenseVariesModeListViewAdapter;
import com.expenshare.app.businesslogic.Budget;
import com.expenshare.app.businesslogic.BusinessLogicTranslator;
import com.expenshare.app.businesslogic.Expense;
import com.expenshare.app.businesslogic.Person;
import com.expenshare.app.businesslogic.PersonBudget;
import com.expenshare.app.businesslogic.PersonExpense;
import com.expenshare.app.businesslogic.Utilities;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dataacces.TableBudgets;
import com.expenshare.app.dataacces.TableExpenses;
import com.expenshare.app.dataacces.TablePersons;
import com.expenshare.app.dataacces.TablePersonsExpenses;
import com.expenshare.app.loaders.PersonsBudgetsJunctionLoader;
import com.expenshare.app.loaders.PersonsLoader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;

public class DialogFragmentAddExpense extends DialogFragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<ArrayList>, View.OnTouchListener, PopupWindow.OnDismissListener, AdapterView.OnItemClickListener {


    private SharedPreferences mSharedPreferences;
    //Widgets
    private Button mCancelButton;
    private Button mOkButton;
    private Button mAddNewPersonButton;
    private Button mChooseExistingPersonButton;
    private Button mNewPersonOkButton;
    private Button mEqualizeButton;
    private Button mEqualBaseButton;
    private EditText mExpenseNameEditText;
    private EditText mExpenseValueEditText;
    private TextView mExpenseValueCurrencyEditText;
    private EditText mNewPersonNameEditText;
    private ListView mListView;
    private TextView mDummySpinnerTextView;
    private ListPopupWindow mListPopupWindow;

    //Adapters
    private AddExpenseListViewBaseAdapter mListViewAdapter;
    private AddExpensePersonsSpinnerAdapter mListPopupWindowAdapter;

    //Data
    private ArrayList<Person> mAllPersonsAvailableInDatabase = new ArrayList<Person>();
    //private ArrayList<Person> mAvailablePersonsNotAlreadyInBudget = new ArrayList<Person>();
    //private ArrayList<Person> mPersonsAvailableInBudget = new ArrayList<Person>();
    private ArrayList<PersonBudget> mPersonsBudgets = new ArrayList<PersonBudget>();

    // private ArrayList<PersonExpense> mExpensePersons = new ArrayList<PersonExpense>();

    //ParentLayouts fo animation use
    private RelativeLayout containerView;
    private RelativeLayout mOptionsLayout;
    private RelativeLayout mNewPersonLayout;

    //Add, choose existing layout indicators
    private boolean mIsAddNewPersonLayoutShown = false;
    private boolean mIsChooseExistingPersonLayoutShown = false;

    //Loaders shit
    private static final String BUDGET_ARG = "budget";
    private static final String LOADER_ARG_BUDGET_ID = "budgetId";
    private static final String LIST_ADAPTER_SHARED_PREF_ARG = "spendingsMode";

    private static final int LOADER_ID_BUDGET_PERSONS = 10;
    private static final int LOADER_ID_ALL_PERSONS = 11;


    private Budget mBudget;
    private Expense mExpense=new Expense();

    private boolean isEqualBaseModeActive;

    public DialogFragmentAddExpense() {
    }

    static public DialogFragmentAddExpense newInstance(Budget budget) {
        DialogFragmentAddExpense f = new DialogFragmentAddExpense();
        Bundle bundle = new Bundle();
        bundle.putParcelable(BUDGET_ARG, budget);
        f.setArguments(bundle);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBudget = getArguments().getParcelable(BUDGET_ARG);
        mExpense = new Expense();
        mExpense.setCurrencyCode(mBudget.getCurrencyCode());
        mExpense.setBudgetId(mBudget.getId());
        mSharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        isEqualBaseModeActive = mSharedPreferences.getBoolean(LIST_ADAPTER_SHARED_PREF_ARG, false);

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_expense_add, null);

        setupControls(view);
        setupOnAddNewPersonLayoutVisibleTouchListeners();
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        startCurrentBudgetPersonsLoader(mBudget);
        startAllAvailablePersonsLoader();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mExpenseNameEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.okButton:
                if (mExpenseNameEditText.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.dialog_fragment_add_expense_empty_name_warning_message), Toast.LENGTH_LONG).show();
                    return;
                } else if (mListViewAdapter.getCount() == 0) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.dialog_fragment_add_expense_no_persons_selected_warning_message), Toast.LENGTH_LONG).show();
                    return;
                } else {
                    addPersonsToDatabase();
                    addExpenseToDatabase();
                    updatePersonsExpensesDatabase();
                    dismiss();
                }
                break;
            case R.id.cancelButton:
                dismiss();

                break;
            case R.id.addNewPersonButton:
                showAddNewPersonLayout();
                break;
            case R.id.chooseExistingPersonButton:
                ArrayList<Person> mAvailablePersonsNotAlreadyInBudget = new ArrayList<Person>(mAllPersonsAvailableInDatabase);
                mAvailablePersonsNotAlreadyInBudget.removeAll(BusinessLogicTranslator.pullPersonsOutOfPersonsExpenses(mListViewAdapter.getPersonsExpensesList()));
                mListPopupWindowAdapter = new AddExpensePersonsSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, mAvailablePersonsNotAlreadyInBudget);

                if (!mAvailablePersonsNotAlreadyInBudget.isEmpty()) {
                    showChooseExistingPersonLayout();
                } else {
                    Toast.makeText(getActivity(), R.string.dialog_fragment_add_expense_no_existing_persons_warning_message, Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.okPersonButton:
                addNewPersonOkButtonClicked();
                break;
            case R.id.buttonEqualBase:
                ArrayList<PersonExpense> personsExpenses = mListViewAdapter.getPersonsExpensesList();
                BigDecimal totalExpense = mListViewAdapter.getTotalExpense();
                if (isEqualBaseModeActive) {
                    mListViewAdapter = new AddExpenseVariesModeListViewAdapter(getActivity(), personsExpenses, totalExpense,DialogFragmentAddExpense.this);
                    mEqualBaseButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.animation_toggle_off, 0, 0, 0);
                    AnimationDrawable equalBaseButtonToggleAnimationDrawable = (AnimationDrawable) mEqualBaseButton.getCompoundDrawables()[0];
                    equalBaseButtonToggleAnimationDrawable.setVisible(true, true);
                    equalBaseButtonToggleAnimationDrawable.start();
                    isEqualBaseModeActive = false;
                } else {

                    mListViewAdapter = new AddExpenseEqualBaseModeListViewAdapter(getActivity(), personsExpenses,totalExpense, DialogFragmentAddExpense.this);
                    mEqualBaseButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.animation_toggle_on, 0, 0, 0);

                    AnimationDrawable equalBaseButtonToggleAnimationDrawable = (AnimationDrawable) mEqualBaseButton.getCompoundDrawables()[0];
                    equalBaseButtonToggleAnimationDrawable.setVisible(true, true);
                    equalBaseButtonToggleAnimationDrawable.start();
                    isEqualBaseModeActive = true;
                }
                mListView.setAdapter(mListViewAdapter);
                mSharedPreferences.edit().putBoolean(LIST_ADAPTER_SHARED_PREF_ARG, isEqualBaseModeActive).commit();

                break;
            case R.id.buttonEqualize:

                mListViewAdapter.equalizeAllExpenses();
                animateEqualizeSpendingsToggle();

                break;

        }

    }

    private void addNewPersonOkButtonClicked(){
        ArrayList<String> allAvailablePersonsNames = new ArrayList<String>();
        for (Person person : mAllPersonsAvailableInDatabase)
            allAvailablePersonsNames.add(person.getName());
        for (Person person : BusinessLogicTranslator.pullPersonsOutOfPersonsExpenses(mListViewAdapter.getPersonsExpensesList())) {
            if (!allAvailablePersonsNames.contains(person.getName()))
                allAvailablePersonsNames.add(person.getName());
        }

        String addedPersonName = mNewPersonNameEditText.getText().toString();
        if (addedPersonName.equals("")) {
            Toast.makeText(getActivity(), R.string.dialog_fragment_add_expense_add_person_name_empty_warning_message, Toast.LENGTH_LONG).show();

        } else if (allAvailablePersonsNames.contains(addedPersonName)) {
            Toast.makeText(getActivity(), R.string.dialog_fragment_add_expense_add_person_person_already_exists_warning_message, Toast.LENGTH_LONG).show();

        } else {
            addNewPersonWithName(addedPersonName);

            hideAddNewPersonLayout();
        }
    }

    private void animateEqualizeSpendingsToggle() {
        final AnimationDrawable equalizeButtonToggleAnimationDrawable = (AnimationDrawable) mEqualizeButton.getCompoundDrawables()[0];
        equalizeButtonToggleAnimationDrawable.setVisible(true, true);
        equalizeButtonToggleAnimationDrawable.start();
    }

    private void addExpenseToDatabase() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableExpenses.COLUMN_NAME, mExpenseNameEditText.getText().toString());
        contentValues.put(TableExpenses.COLUMN_BUDGET_ID, mBudget.getId());
        contentValues.put(TableExpenses.COLUMN_DATE, Utilities.getCurrentDate());
        contentValues.put(TableExpenses.COLUMN_MONEY_X_100, Utilities.getIntX100FromBigDecimal(Utilities.getBigDecimalFromString(mExpenseValueEditText.getText().toString())));
        mExpense.setId(ContentUris.parseId(getActivity().getContentResolver().insert(MyContentProvider.CONTENT_URI_EXPENSES, contentValues)));

    }


    private void addPersonsToDatabase() {
        for (Person person : BusinessLogicTranslator.pullPersonsOutOfPersonsExpenses(mListViewAdapter.getPersonsExpensesList())) {
            if (!mAllPersonsAvailableInDatabase.contains(person)) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(TablePersons.COLUMN_NAME, person.getName());
                person.setId(ContentUris.parseId(getActivity().getContentResolver().insert(MyContentProvider.CONTENT_URI_PERSONS, contentValues)));
            }
        }
    }

    private void updatePersonsExpensesDatabase() {

        for (PersonExpense personExpense : mListViewAdapter.getPersonsExpensesList()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(TablePersonsExpenses.COLUMN_EXPENSE_ID, mExpense.getId());
            contentValues.put(TablePersonsExpenses.COLUMN_PERSON_ID, personExpense.getPerson().getId());
            contentValues.put(TablePersonsExpenses.COLUMN_PAYMENT_X_100, Utilities.getIntX100FromBigDecimal(personExpense.getPersonPayment()));
            contentValues.put(TablePersonsExpenses.COLUMN_SPENDING_X_100, Utilities.getIntX100FromBigDecimal(personExpense.getPersonSpending()));
            contentValues.put(TablePersonsExpenses.COLUMN_PAYMENT_LOCKED, personExpense.isPersonPaymentLocked() ? 1 : 0);
            contentValues.put(TablePersonsExpenses.COLUMN_SPENDING_LOCKED, personExpense.isPersonSpendingLocked() ? 1 : 0);

            getActivity().getContentResolver().insert(MyContentProvider.CONTENT_URI_PERSONS_EXPENSES, contentValues);
        }
    }

    private void setupControls(View view) {
        containerView = (RelativeLayout) view.findViewById(R.id.containerView);

        mNewPersonLayout = (RelativeLayout) view.findViewById(R.id.newPersonNameEditTextContainer);
        mOptionsLayout = (RelativeLayout) view.findViewById(R.id.optionsContainer);

        mExpenseNameEditText = (EditText) view.findViewById(R.id.expenseNameEditText);
        mExpenseValueEditText = (EditText) view.findViewById(R.id.expenseValueEditText);
        mExpenseValueEditText.setText("0.00");
        setupExpenseValueEditTextListener(mExpenseValueEditText);
        setupExpenseValueEditTextFocusListener(mExpenseValueEditText);
        mNewPersonNameEditText = (EditText) view.findViewById(R.id.newPersonNameEditText);
        mNewPersonNameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    addNewPersonOkButtonClicked();
                }
                return false;
            }
        });
        mCancelButton = (Button) view.findViewById(R.id.cancelButton);
        mCancelButton.setOnClickListener(this);
        mOkButton = (Button) view.findViewById(R.id.okButton);
        mOkButton.setOnClickListener(this);

        mNewPersonOkButton = (Button) view.findViewById(R.id.okPersonButton);
        mNewPersonOkButton.setOnClickListener(this);

        mAddNewPersonButton = (Button) view.findViewById(R.id.addNewPersonButton);
        mAddNewPersonButton.setOnClickListener(this);
        mChooseExistingPersonButton = (Button) view.findViewById(R.id.chooseExistingPersonButton);
        mChooseExistingPersonButton.setOnClickListener(this);

        mEqualizeButton = (Button) view.findViewById(R.id.buttonEqualize);
        mEqualizeButton.setOnClickListener(this);
        mEqualBaseButton = (Button) view.findViewById(R.id.buttonEqualBase);
        mEqualBaseButton.setOnClickListener(this);
        if(isEqualBaseModeActive)
            mEqualBaseButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_toggle_on,0,0,0);
        else
            mEqualBaseButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_toggle_off,0,0,0);

        mExpenseValueCurrencyEditText=(TextView) view.findViewById(R.id.expenseValueCurrencyTextView);
        mExpenseValueCurrencyEditText.setText(Currency.getInstance(mBudget.getCurrencyCode()).getSymbol());

        mListView = (ListView) view.findViewById(R.id.personsExpensesListView);
        mDummySpinnerTextView = (TextView) view.findViewById(R.id.choosePersonSpinnerTextView);

    }

    private void setupExpenseValueEditTextListener(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {
                                                editText.removeTextChangedListener(this);

                                                BigDecimal parsed = Utilities.getBigDecimalFromString(editable.toString());
                                                editText.setText(parsed.toString());
                                                editText.setSelection(parsed.toString().length());
                                                mListViewAdapter.setTotalExpense(parsed);
                                                editText.addTextChangedListener(this);
                                            }
                                        }
        );
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                editText.setSelection(editText.getText().length());
            }
        });
    }

    private void setupExpenseValueEditTextFocusListener(final EditText editText) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                editText.setSelection(editText.getText().length());
            }
        });
    }


    private void setupOnAddNewPersonLayoutVisibleTouchListeners() {
        containerView.setOnTouchListener(this);
        mExpenseNameEditText.setOnTouchListener(this);
        mExpenseValueEditText.setOnTouchListener(this);
        mCancelButton.setOnTouchListener(this);
        mOkButton.setOnTouchListener(this);
        mAddNewPersonButton.setOnTouchListener(this);
        mListView.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Rect addNewPersonLayoutTouchRect = new Rect();
        mNewPersonLayout.getHitRect(addNewPersonLayoutTouchRect);
        if (mIsAddNewPersonLayoutShown) {
            if (addNewPersonLayoutTouchRect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return false;
            } else {
                hideAddNewPersonLayout();
                return true;
            }

        }
        return false;
    }


    private void startCurrentBudgetPersonsLoader(Budget budget) {
        Bundle bundle = new Bundle();
        bundle.putString(LOADER_ARG_BUDGET_ID, String.valueOf(budget.getId()));
        getLoaderManager().initLoader(LOADER_ID_BUDGET_PERSONS, bundle, this);
    }

    private void startAllAvailablePersonsLoader() {
        getLoaderManager().initLoader(LOADER_ID_ALL_PERSONS, null, this);
    }

    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_ID_BUDGET_PERSONS:
                return new PersonsBudgetsJunctionLoader(getActivity(), MyContentProvider.CONTENT_URI_BUDGET_PERSONS, null, TableBudgets.COLUMN_ID + "=?", new String[]{args.getString(LOADER_ARG_BUDGET_ID)}, null);

            case LOADER_ID_ALL_PERSONS:
                return new PersonsLoader(getActivity(), MyContentProvider.CONTENT_URI_PERSONS, null, null, null, null);
            default:
                return null;
        }

    }


    @Override
    public void onLoadFinished(final Loader<ArrayList> loader,  ArrayList data) {
        switch (loader.getId()) {
            case LOADER_ID_BUDGET_PERSONS:
                mPersonsBudgets = (ArrayList<PersonBudget>) data;
                mListView.post(new Runnable() {
                    @Override
                    public void run() {

                        ArrayList<PersonExpense> personsExpenses = new ArrayList<PersonExpense>(mPersonsBudgets.size());
                        for (PersonBudget personBudget : mPersonsBudgets) {
                            PersonExpense personExpense = new PersonExpense();
                            personExpense.setPerson(personBudget.getPerson());
                            personExpense.setExpense(mExpense);
                            personsExpenses.add(personExpense);
                        }
                        if (!isEqualBaseModeActive)
                            mListViewAdapter = new AddExpenseVariesModeListViewAdapter(getActivity(), personsExpenses, BigDecimal.ZERO,  DialogFragmentAddExpense.this);
                        else
                            mListViewAdapter = new AddExpenseEqualBaseModeListViewAdapter(getActivity(), personsExpenses, BigDecimal.ZERO, DialogFragmentAddExpense.this);


                        mListView.setAdapter(mListViewAdapter);
                        getLoaderManager().destroyLoader(LOADER_ID_BUDGET_PERSONS);
                    }
                });
                break;

            case LOADER_ID_ALL_PERSONS:
                mAllPersonsAvailableInDatabase = (ArrayList<Person>) data;
                getLoaderManager().destroyLoader(LOADER_ID_ALL_PERSONS);
                break;
        }


    }

    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }


    // Adding persons layouts animation and logic methods

    private void addNewPersonWithName(String name) {
        Person person = new Person();
        person.setName(name);
        PersonExpense personExpense = new PersonExpense();
        personExpense.setPerson(person);
        personExpense.setExpense(mExpense);
        mListViewAdapter.add(personExpense);
    }


    private void showAddNewPersonLayout() {

        if (!mIsAddNewPersonLayoutShown) {
            mIsAddNewPersonLayoutShown = true;
            float addButtonAfterLayoutLeftEdge = ((float) containerView.getWidth()) / 2 - ((float) mAddNewPersonButton.getWidth()) / 2;
            float addButtonBeforeLayoutLeftEdge = mAddNewPersonButton.getX();
            float addButtonTranslationX = addButtonAfterLayoutLeftEdge - addButtonBeforeLayoutLeftEdge;
            ObjectAnimator addButtonAnimator = ObjectAnimator.ofFloat(mAddNewPersonButton, "translationX", addButtonTranslationX);


            float chooseButtonAfterLayoutLeftEdge = ((float) containerView.getWidth());
            float chooseButtonBeforeLayoutLeftEdge = mChooseExistingPersonButton.getX();
            float chooseButtonTranslationX = chooseButtonAfterLayoutLeftEdge - chooseButtonBeforeLayoutLeftEdge;
            ObjectAnimator chooseButtonAnimator = ObjectAnimator.ofFloat(mChooseExistingPersonButton, "translationX", chooseButtonTranslationX);


            mDummySpinnerTextView.setClickable(false);

            mNewPersonLayout.setVisibility(View.VISIBLE);
            //  mNewPersonLayout.setX(-mChooseExistingPersonButton.getWidth());
            float newPersonLayoutAfterAnimationLeftEdge = containerView.getX();
            float newPersonLayoutBeforeAnimationLeftEdge = (-containerView.getWidth());
            ObjectAnimator mNewPersonEditTextParentViewAnimator = ObjectAnimator.ofFloat(mNewPersonLayout, "translationX", newPersonLayoutBeforeAnimationLeftEdge, newPersonLayoutAfterAnimationLeftEdge);

            float optionsLayoutAfterAnimationLeftEdge = mOptionsLayout.getWidth();
            float optionsLayoutBeforeAnimationLeftEdge = mOptionsLayout.getX();
            ObjectAnimator optionsLayoutAnimator = ObjectAnimator.ofFloat(mOptionsLayout, "translationX", optionsLayoutBeforeAnimationLeftEdge, optionsLayoutAfterAnimationLeftEdge);

            AnimatorSet set = new AnimatorSet();
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.playTogether(addButtonAnimator, chooseButtonAnimator, mNewPersonEditTextParentViewAnimator, optionsLayoutAnimator);
            set.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    if (mNewPersonNameEditText.requestFocus()) {
                        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(mNewPersonNameEditText, InputMethodManager.SHOW_IMPLICIT);
                    }

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            set.start();
        }
    }

    private void hideAddNewPersonLayout() {
        if (mIsAddNewPersonLayoutShown) {
            mIsAddNewPersonLayoutShown = false;
            ObjectAnimator addButtonAnimator = ObjectAnimator.ofFloat(mAddNewPersonButton, "translationX", 0);
            ObjectAnimator chooseButtonAnimator = ObjectAnimator.ofFloat(mChooseExistingPersonButton, "translationX", 0);

            float newPersonEditContainerLayoutBeforeLeftEdge = containerView.getX();
            float newPersonEditContainerLayoutAfterLeftEdge = (-containerView.getWidth());
            ObjectAnimator mNewPersonEditTextParentViewAnimator = ObjectAnimator.ofFloat(mNewPersonLayout, "translationX", newPersonEditContainerLayoutBeforeLeftEdge, newPersonEditContainerLayoutAfterLeftEdge);


            ObjectAnimator optionsLayoutAnimator = ObjectAnimator.ofFloat(mOptionsLayout, "translationX", 0);


            AnimatorSet set = new AnimatorSet();
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.playTogether(addButtonAnimator, chooseButtonAnimator, mNewPersonEditTextParentViewAnimator, optionsLayoutAnimator);
            set.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    mNewPersonLayout.setVisibility(View.INVISIBLE);
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(mNewPersonLayout.getWindowToken(), 0);
                    mNewPersonNameEditText.setText("");


                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            set.start();
        }
    }

    private void showChooseExistingPersonLayout() {

        if (!mIsChooseExistingPersonLayoutShown) {
            mIsChooseExistingPersonLayoutShown = true;
            float chooseButtonAfterLayoutLeftEdge = ((float) containerView.getWidth()) / 2 - ((float) mChooseExistingPersonButton.getWidth()) / 2;
            float chooseButtonBeforeLayoutLeftEdge = mChooseExistingPersonButton.getX();
            float chooseButtonTranslationX = chooseButtonAfterLayoutLeftEdge - chooseButtonBeforeLayoutLeftEdge;
            ObjectAnimator addButtonAnimator = ObjectAnimator.ofFloat(mChooseExistingPersonButton, "translationX", chooseButtonTranslationX);


            float addButtonAfterLayoutLeftEdge = ((float) -containerView.getWidth());
            float addButtonBeforeLayoutLeftEdge = mAddNewPersonButton.getX();
            float addButtonTranslationX = addButtonAfterLayoutLeftEdge - addButtonBeforeLayoutLeftEdge;
            ObjectAnimator chooseButtonAnimator = ObjectAnimator.ofFloat(mAddNewPersonButton, "translationX", addButtonTranslationX);


            mDummySpinnerTextView.setVisibility(View.VISIBLE);
            //  mNewPersonLayout.setX(-mChooseExistingPersonButton.getWidth());
            float chooseExistingSpinnerAfterLeftEdge = containerView.getX();
            float chooseExistingSpinnerBeforeLeftEdge = containerView.getWidth();
            ObjectAnimator mNewPersonEditTextParentViewAnimator = ObjectAnimator.ofFloat(mDummySpinnerTextView, "translationX", chooseExistingSpinnerBeforeLeftEdge, chooseExistingSpinnerAfterLeftEdge);

            float optionsLayoutAfterAnimationLeftEdge = -mOptionsLayout.getWidth();
            float optionsLayoutBeforeAnimationLeftEdge = mOptionsLayout.getX();
            ObjectAnimator optionsLayoutAnimator = ObjectAnimator.ofFloat(mOptionsLayout, "translationX", optionsLayoutBeforeAnimationLeftEdge, optionsLayoutAfterAnimationLeftEdge);


            AnimatorSet set = new AnimatorSet();
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.playTogether(addButtonAnimator, chooseButtonAnimator, mNewPersonEditTextParentViewAnimator, optionsLayoutAnimator);
            set.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {

                    mListPopupWindow = new ListPopupWindow(getActivity());
                    mListPopupWindow.setAdapter(mListPopupWindowAdapter);
                    mListPopupWindow.setModal(true);
                    mListPopupWindow.setContentWidth(mDummySpinnerTextView.getWidth());
                    mListPopupWindow.setAnchorView(mDummySpinnerTextView);
                    mListPopupWindow.setOnItemClickListener(DialogFragmentAddExpense.this);
                    mListPopupWindow.show();
                    mListPopupWindow.setOnDismissListener(DialogFragmentAddExpense.this);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            set.start();
        }
    }

    private void hideChooseExistingPersonLayout() {

        if (mIsChooseExistingPersonLayoutShown) {
            mIsChooseExistingPersonLayoutShown = false;
            ObjectAnimator addButtonAnimator = ObjectAnimator.ofFloat(mAddNewPersonButton, "translationX", 0);
            ObjectAnimator chooseButtonAnimator = ObjectAnimator.ofFloat(mChooseExistingPersonButton, "translationX", 0);


            //  mNewPersonLayout.setX(-mChooseExistingPersonButton.getWidth());
            float chooseExistingSpinnerAfterLeftEdge = mDummySpinnerTextView.getWidth();
            float chooseExistingSpinnerBeforeLeftEdge = mDummySpinnerTextView.getX();
            ObjectAnimator mNewPersonEditTextParentViewAnimator = ObjectAnimator.ofFloat(mDummySpinnerTextView, "translationX", chooseExistingSpinnerBeforeLeftEdge, chooseExistingSpinnerAfterLeftEdge);


            ObjectAnimator optionsLayoutAnimator = ObjectAnimator.ofFloat(mOptionsLayout, "translationX", 0);

            AnimatorSet set = new AnimatorSet();
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.playTogether(addButtonAnimator, chooseButtonAnimator, mNewPersonEditTextParentViewAnimator, optionsLayoutAnimator);
            set.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {

                    mDummySpinnerTextView.setVisibility(View.INVISIBLE);

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            set.start();
        }
    }

    @Override
    public void onDismiss() {
        hideChooseExistingPersonLayout();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        PersonExpense personExpense = new PersonExpense();
        personExpense.setPerson(mListPopupWindowAdapter.getItem(i));
        personExpense.setExpense(mExpense);
        mListViewAdapter.add(personExpense);
        mListPopupWindow.dismiss();
    }


}