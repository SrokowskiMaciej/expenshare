package com.expenshare.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;


import com.expenshare.app.adapters.BudgetPersonsListViewAdapter;
import com.expenshare.app.businesslogic.Budget;
import com.expenshare.app.businesslogic.PersonBudget;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dataacces.TableBudgets;
import com.expenshare.app.loaders.PersonsBudgetsJunctionLoader;

import java.util.ArrayList;

/**
 * Created by Maciek on 2014-05-11.
 */
public class BudgetPersonsFragment extends ListFragment implements   LoaderManager.LoaderCallbacks<ArrayList> {

    private static final String BUDGET_ARG = "budget";
    private static final String LOADER_ARG_BUDGET_ID = "budgetId";
    private static final int LOADER_ID = 21;

    private BudgetPersonsDialogsListener mListener;



    private Budget mBudget;
    private ArrayList<PersonBudget> mPersonsBudgets = new ArrayList<PersonBudget>();

    public static BudgetPersonsFragment newInstance(Budget budget) {
        BudgetPersonsFragment fragment = new BudgetPersonsFragment();
        Bundle args = new Bundle();
        args.putParcelable(BUDGET_ARG, budget);
        fragment.setArguments(args);
        return fragment;
    }

    public BudgetPersonsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBudget = getArguments().getParcelable(BUDGET_ARG);
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onResume() {
        super.onResume();
        if(mBudget!=null)
            startLoader(mBudget);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (BudgetPersonsDialogsListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement BudgetPersonsDialogsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }




    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        mListener.showBudgetPersonDialog(((BudgetPersonsListViewAdapter)getListAdapter()).getItem(position));
    }



    private void startLoader(Budget budget){
        Bundle bundle=new Bundle();
        bundle.putString(LOADER_ARG_BUDGET_ID, String.valueOf(budget.getId()));
        getLoaderManager().initLoader(LOADER_ID,bundle,this);
    }

    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {
              return new PersonsBudgetsJunctionLoader(getActivity(), MyContentProvider.CONTENT_URI_BUDGET_PERSONS, null, TableBudgets.COLUMN_ID + "=?", new String[]{args.getString(LOADER_ARG_BUDGET_ID)}, null);

    }
        @Override
    public void onLoadFinished(final Loader<ArrayList> loader, final ArrayList data) {

        getListView().post(new Runnable() {
            @Override
            public void run() {
                mPersonsBudgets = (ArrayList<PersonBudget>) data;
                setListAdapter(new BudgetPersonsListViewAdapter(getActivity(),
                        R.layout.cell_list_view_budget_persons, mPersonsBudgets, mBudget.getCurrencyCode()));
            }
        });

    }

    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }



    public interface BudgetPersonsDialogsListener {

        public void showBudgetPersonDialog(PersonBudget personBudget);

    }


}
