package com.expenshare.app.loaders;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.expenshare.app.businesslogic.Expense;
import com.expenshare.app.businesslogic.Person;
import com.expenshare.app.businesslogic.PersonExpense;
import com.expenshare.app.businesslogic.Utilities;
import com.expenshare.app.dataacces.TableExpenses;
import com.expenshare.app.dataacces.TablePersons;
import com.expenshare.app.dataacces.TablePersonsExpenses;

import java.util.ArrayList;

public class PersonsExpensesLoader extends BaseLoader{
    public PersonsExpensesLoader(Context context, Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        super(context, uri, projection, selection, selectionArgs, sortOrder);
    }

    private Person person=null;
    private Expense expense=null;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Expense getExpense() {
        return expense;
    }

    public void setExpense(Expense expense) {
        this.expense = expense;
    }

    @Override
    public ArrayList loadInBackground() {

        Cursor cursor=getNewCursor();

        ArrayList<PersonExpense> personsExpenses = new ArrayList<PersonExpense>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    PersonExpense personExpense=new PersonExpense();

                    if (person == null) {
                        Person databasePerson = new Person();
                        databasePerson.setName(cursor.getString(cursor.getColumnIndex(TablePersons.COLUMN_NAME)));
                        databasePerson.setId(cursor.getLong(cursor.getColumnIndex(TablePersons.COLUMN_ID)));
                        personExpense.setPerson(databasePerson);
                    } else
                        personExpense.setPerson(person);
                    if (expense == null) {
                        Expense databaseExpense = new Expense();
                        databaseExpense.setName(cursor.getString(cursor.getColumnIndex(TableExpenses.COLUMN_NAME)));
                        databaseExpense.setDate(cursor.getString(cursor.getColumnIndex(TableExpenses.COLUMN_DATE)));
                        databaseExpense.setId(cursor.getLong(cursor.getColumnIndex(TableExpenses.COLUMN_ID)));
                        databaseExpense.setBudgetId(cursor.getLong(cursor.getColumnIndex(TableExpenses.COLUMN_BUDGET_ID)));
                        databaseExpense.setTotalExpense(Utilities.getBigDecimalFromIntX100(cursor.getInt(cursor.getColumnIndex(TableExpenses.COLUMN_MONEY_X_100))));
                        databaseExpense.setNumberOfPersons(getExpensePersonCount(databaseExpense.getId()));
                        databaseExpense.setCurrencyCode(getBudgetCurrencyCode(databaseExpense.getBudgetId()));
                        personExpense.setExpense(databaseExpense);

                    } else
                        personExpense.setExpense(expense);

                    personExpense.setPersonPayment(Utilities.getBigDecimalFromIntX100(cursor.getInt(cursor.getColumnIndex(TablePersonsExpenses.COLUMN_PAYMENT_X_100))));
                    personExpense.setPersonSpending(Utilities.getBigDecimalFromIntX100(cursor.getInt(cursor.getColumnIndex(TablePersonsExpenses.COLUMN_SPENDING_X_100))));
                    personExpense.setPersonPaymentLocked(cursor.getInt(cursor.getColumnIndex(TablePersonsExpenses.COLUMN_PAYMENT_LOCKED))==1 ? true:false);
                    personExpense.setPersonSpendingLocked(cursor.getInt(cursor.getColumnIndex(TablePersonsExpenses.COLUMN_SPENDING_LOCKED))==1 ? true:false);
                    personExpense.setId(cursor.getInt(cursor.getColumnIndex(TablePersonsExpenses.COLUMN_ID)));
                    personsExpenses.add(personExpense);
                }
                while (cursor.moveToNext());
            }
        }
        cursor.close();
        return personsExpenses;
    }
}
