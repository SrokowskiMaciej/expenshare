package com.expenshare.app.loaders;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.expenshare.app.businesslogic.Expense;
import com.expenshare.app.businesslogic.Utilities;
import com.expenshare.app.dataacces.TableExpenses;

import java.util.ArrayList;

public class ExpensesLoader extends BaseLoader {

    public ExpensesLoader(Context context, Uri uri, String[] projection, String selection,
                         String[] selectionArgs, String sortOrder) {
        super(context, uri, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public ArrayList loadInBackground() {
        Cursor cursor = getNewCursor();

        ArrayList<Expense> expenses = new ArrayList<Expense>();
        if (cursor != null) {

            if (cursor.moveToFirst()) {
                do {
                    Expense expense = new Expense();
                    expense.setName(cursor.getString(cursor.getColumnIndex(TableExpenses.COLUMN_NAME)));
                    expense.setDate(cursor.getString(cursor.getColumnIndex(TableExpenses.COLUMN_DATE)));
                    expense.setId(cursor.getLong(cursor.getColumnIndex(TableExpenses.COLUMN_ID)));
                    expense.setBudgetId(cursor.getLong(cursor.getColumnIndex(TableExpenses.COLUMN_BUDGET_ID)));
                    expense.setTotalExpense(Utilities.getBigDecimalFromIntX100(cursor.getInt(cursor.getColumnIndex(TableExpenses.COLUMN_MONEY_X_100))));
                    expense.setNumberOfPersons(getExpensePersonCount(expense.getId()));
                    expense.setCurrencyCode(getBudgetCurrencyCode(expense.getBudgetId()));
                    expenses.add(expense);

                }
                while (cursor.moveToNext());
            }
            //cursor.close();

        }
        cursor.close();
        return expenses;
    }
}
