package com.expenshare.app.loaders;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.expenshare.app.businesslogic.Person;
import com.expenshare.app.dataacces.TablePersons;

import java.util.ArrayList;

public class PersonsLoader extends BaseLoader {


    public PersonsLoader(Context context, Uri uri, String[] projection, String selection,
                         String[] selectionArgs, String sortOrder) {
        super(context, uri, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public ArrayList loadInBackground() {
        Cursor cursor = getNewCursor();

        ArrayList<Person> persons = new ArrayList<Person>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Person person = new Person();
                    person.setName(cursor.getString(cursor.getColumnIndex(TablePersons.COLUMN_NAME)));
                    person.setId(cursor.getLong(cursor.getColumnIndex(TablePersons.COLUMN_ID)));
                    persons.add(person);

                }
                while (cursor.moveToNext());
            }
            //cursor.close();

        }
        cursor.close();
        return persons;
    }
}
