package com.expenshare.app.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.adapters.PersonsExpensesAdapter;
import com.expenshare.app.businesslogic.PersonBudget;
import com.expenshare.app.businesslogic.PersonExpense;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dataacces.TableExpenses;
import com.expenshare.app.dataacces.TablePersons;
import com.expenshare.app.loaders.PersonsExpensesLoader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;

/**
 * Created by Maciek on 2014-06-27.
 */
public class DialogFragmentShowBudgetPerson extends DialogFragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<ArrayList> {

    private Button mOkButton;
    private TextView mPersonNameTextView;
    private TextView mBudgetNameTextView;
    private TextView mBudgetTotalExpenseTextView;
    private TextView mBudgetTotalExpenseCurrencyTextView;
    private TextView mPersonSpendingInBudgetTextView;
    private TextView mPersonPaymentInBudgetTextView;
    private TextView mPersonSpendingInBudgetCurrencyTextView;
    private TextView mPersonPaymentInBudgetCurrencyTextView;

    private TextView mTotalPersonCommitmentTextView;
    private TextView mTotalPersonCommitmentCurrencyTextView;

    private ListView mListView;

    private PersonBudget mPersonBudget;

    //Adapters
    private PersonsExpensesAdapter mListViewAdapter;

    //Data
    private static final String PERSON_BUDGET_ARG = "person_budget";

    public DialogFragmentShowBudgetPerson() {
    }

    static public DialogFragmentShowBudgetPerson newInstance(PersonBudget personBudget) {
        DialogFragmentShowBudgetPerson f = new DialogFragmentShowBudgetPerson();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PERSON_BUDGET_ARG, personBudget);
        f.setArguments(bundle);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPersonBudget = getArguments().getParcelable(PERSON_BUDGET_ARG);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_show_budget_person, null);

        setupControls(view);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(453, null, this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.okButton:

                dismiss();
                break;
        }

    }


    private void setupControls(View view) {

        mPersonNameTextView = (TextView) view.findViewById(R.id.titleTextView);
        mPersonNameTextView.setText(mPersonBudget.getPerson().getName());

        mBudgetNameTextView = (TextView) view.findViewById(R.id.budgetNameTextView);
        mBudgetNameTextView.setText(mPersonBudget.getBudget().getName());

        mBudgetTotalExpenseTextView = (TextView) view.findViewById(R.id.budgetTotalExpenseTextView);
        mBudgetTotalExpenseTextView.setText(mPersonBudget.getBudget().getTotalExpenses().toString());


        mBudgetTotalExpenseCurrencyTextView = (TextView) view.findViewById(R.id.budgetTotalExpenseCurrencyTextView);
        mBudgetTotalExpenseCurrencyTextView.setText(Currency.getInstance(mPersonBudget.getBudget().getCurrencyCode()).getSymbol());

        mPersonSpendingInBudgetTextView = (TextView) view.findViewById(R.id.personSpendingTextView);
        mPersonSpendingInBudgetTextView.setText(mPersonBudget.getPersonSpending().toString());

        mPersonPaymentInBudgetTextView = (TextView) view.findViewById(R.id.personPaymentTextView);
        mPersonPaymentInBudgetTextView.setText(mPersonBudget.getPersonPayment().toString());

        mPersonSpendingInBudgetCurrencyTextView = (TextView) view.findViewById(R.id.personSpendingCurrencyCodeTextView);
        mPersonSpendingInBudgetCurrencyTextView.setText(Currency.getInstance(mPersonBudget.getBudget().getCurrencyCode()).getSymbol());


        mPersonPaymentInBudgetCurrencyTextView = (TextView) view.findViewById(R.id.personPaymentCurrencyCodeTextView);
        mPersonPaymentInBudgetCurrencyTextView.setText(Currency.getInstance(mPersonBudget.getBudget().getCurrencyCode()).getSymbol());



        mTotalPersonCommitmentCurrencyTextView= (TextView) view.findViewById(R.id.totalPersonCommitmentCurrencyTextView);
        mTotalPersonCommitmentCurrencyTextView.setText(Currency.getInstance(mPersonBudget.getBudget().getCurrencyCode()).getSymbol());



        mTotalPersonCommitmentTextView= (TextView) view.findViewById(R.id.totalPersonCommitmentTextView);
        BigDecimal totalDifference=mPersonBudget.getPersonPayment().subtract(mPersonBudget.getPersonSpending());

        if(totalDifference.signum()==-1)
            mTotalPersonCommitmentTextView.setTextColor(getResources().getColor(R.color.red));
        else
            mTotalPersonCommitmentTextView.setTextColor(getResources().getColor(R.color.green));
        mTotalPersonCommitmentTextView.setText(totalDifference.toString());


        mOkButton = (Button) view.findViewById(R.id.okButton);
        mOkButton.setOnClickListener(this);

        mListView = (ListView) view.findViewById(R.id.personsExpensesListView);

    }


    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {

        String[] selectionArgs=new String[] {String.valueOf(mPersonBudget.getBudget().getId()),String.valueOf(mPersonBudget.getPerson().getId())};
        return new PersonsExpensesLoader(getActivity(), MyContentProvider.CONTENT_URI_PERSONS_EXPENSES, null, TableExpenses.COLUMN_BUDGET_ID + "=? AND "+ TablePersons.COLUMN_ID+"=?", selectionArgs, null);


    }


    @Override
    public void onLoadFinished(final Loader<ArrayList> loader, final ArrayList data) {

        mListView.post(new Runnable() {
            @Override
            public void run() {
                ArrayList<PersonExpense> personExpenses = (ArrayList<PersonExpense>) data;
                mListViewAdapter = new PersonsExpensesAdapter(getActivity(), R.layout.cell_list_view_persons_expense,  personExpenses);
                mListView.setAdapter(mListViewAdapter);

            }


        });

    }

    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }
}

