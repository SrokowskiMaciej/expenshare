package com.expenshare.app.loaders;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.expenshare.app.businesslogic.Budget;
import com.expenshare.app.businesslogic.Person;
import com.expenshare.app.businesslogic.PersonBudget;
import com.expenshare.app.dataacces.TableBudgets;
import com.expenshare.app.dataacces.TablePersons;

import java.util.ArrayList;

public class PersonsBudgetsJunctionLoader extends BaseLoader {

    private Person person = null;
    private Budget budget = null;

    public PersonsBudgetsJunctionLoader(Context context, Uri uri, String[] projection, String selection,
                                        String[] selectionArgs, String sortOrder) {
        super(context, uri, projection, selection, selectionArgs, sortOrder);
    }


    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget expense) {
        this.budget = expense;
    }

    @Override
    public ArrayList loadInBackground() {


        ArrayList<PersonBudget> personsBudgets = new ArrayList<PersonBudget>();
            Cursor cursor = getNewCursor();


            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        PersonBudget personBudget = new PersonBudget();

                        if (person == null) {
                            Person databasePerson = new Person();
                            databasePerson.setName(cursor.getString(cursor.getColumnIndex(TablePersons.COLUMN_NAME)));
                            databasePerson.setId(cursor.getLong(cursor.getColumnIndex(TablePersons.COLUMN_ID)));
                            personBudget.setPerson(databasePerson);
                        } else
                            personBudget.setPerson(person);
                        if (budget == null) {
                            Budget budget = new Budget();
                            budget.setName(cursor.getString(cursor.getColumnIndex(TableBudgets.COLUMN_NAME)));
                            budget.setDescription(cursor.getString(cursor.getColumnIndex(TableBudgets.COLUMN_DESCRIPTION)));
                            budget.setId(cursor.getLong(cursor.getColumnIndex(TableBudgets.COLUMN_ID)));
                            budget.setCurrencyCode(cursor.getString(cursor.getColumnIndex(TableBudgets.COLUMN_CURRENCY_CODE)));
                            budget.setTotalExpenses(getBudgetTotalExpense(budget.getId()));
                            budget.setNumberOfPersons(getBudgetPersonCount(budget.getId()));
                            personBudget.setBudget(budget);
                        } else
                            personBudget.setBudget(budget);

                        personBudget.setPersonPayment(getPersonBudgetPayment(personBudget.getBudget().getId(),personBudget.getPerson().getId()));
                        personBudget.setPersonSpending(getPersonBudgetSpending(personBudget.getBudget().getId(),personBudget.getPerson().getId()));
                        personsBudgets.add(personBudget);
                    }
                    while (cursor.moveToNext());
                }
            }
        cursor.close();
        return personsBudgets;

    }
}
