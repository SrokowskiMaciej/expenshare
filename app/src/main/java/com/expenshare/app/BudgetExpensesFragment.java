package com.expenshare.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.expenshare.app.adapters.ExpensesListViewAdapter;
import com.expenshare.app.businesslogic.Budget;
import com.expenshare.app.businesslogic.Expense;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dataacces.TableExpenses;
import com.expenshare.app.loaders.ExpensesLoader;

import java.util.ArrayList;


public class BudgetExpensesFragment extends ListFragment implements AbsListView.MultiChoiceModeListener,   LoaderManager.LoaderCallbacks<ArrayList> {

    private static final String BUDGET_PARAM = "budget";

    private static final String LOADER_ARG_BUDGET_ID = "budgetId";
    private static final int LOADER_ID = 20;


    private Budget mBudget;
    private ArrayList<Expense> mExpenses = new ArrayList<Expense>();
    BudgetExpensesDialogsListener mListener;

    public static BudgetExpensesFragment newInstance(Budget budget) {
        BudgetExpensesFragment fragment = new BudgetExpensesFragment();
        Bundle args = new Bundle();
        args.putParcelable(BUDGET_PARAM, budget);
        fragment.setArguments(args);
        return fragment;
    }

    public BudgetExpensesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBudget = getArguments().getParcelable(BUDGET_PARAM);
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setDivider(getResources().getDrawable(R.color.primary_dark));
        getListView().setDividerHeight(1);

    }


    @Override
    public void onResume() {
        super.onResume();
        if (mBudget != null)
            startLoader(mBudget);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (BudgetExpensesDialogsListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement BudgetExpensesDialogsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        mListener.showExpenseDialog(((ExpensesListViewAdapter) getListAdapter()).getItem(position));

    }



    private void startLoader(Budget budget) {
        Bundle bundle = new Bundle();
        bundle.putString(LOADER_ARG_BUDGET_ID, String.valueOf(budget.getId()));
        getLoaderManager().initLoader(LOADER_ID, bundle, this);
    }

    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {

        return new ExpensesLoader(getActivity(), MyContentProvider.CONTENT_URI_EXPENSES, null, TableExpenses.COLUMN_BUDGET_ID + "=?", new String[]{args.getString(LOADER_ARG_BUDGET_ID)}, null);

    }

    @Override
    public void onLoadFinished(final Loader<ArrayList> loader, final ArrayList data) {
        getListView().post(new Runnable() {
            @Override
            public void run() {
                mExpenses = (ArrayList<Expense>) data;
                setListAdapter(new ExpensesListViewAdapter(getActivity(),
                        R.layout.cell_list_view_budget_expenses, mExpenses));
                getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
                getListView().setMultiChoiceModeListener(BudgetExpensesFragment.this);
            }
        });
    }


    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }


    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.budget_expenses_context_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        menu.clear();
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.budget_expenses_context_menu, menu);
        if (getListView().getCheckedItemCount() > 1) {
            menu.removeItem(R.id.edit_expense_item);
        }
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.edit_expense_item:
                SparseBooleanArray array=getListView().getCheckedItemPositions();
                int position =array.keyAt(array.indexOfValue(true));
                mListener.showEditExpenseDialog((Expense) getListAdapter().getItem(position));

                //editBudget((Budget) getListAdapter().getItem(getListView().getCheckedItemPosition()));

                return true;
            case R.id.delete_expense_item:

                SparseBooleanArray checkedItemsPositions=getListView().getCheckedItemPositions();
                ArrayList<Expense> checkedExpenses=new ArrayList<Expense>();
                for(int i=0 ;i<getListView().getCount();i++){
                    if(checkedItemsPositions.get(i))
                        checkedExpenses.add((Expense)getListAdapter().getItem(i));
                }
                mListener.showDeleteExpenseDialog(checkedExpenses);
                //deleteBudget((Budget) getListAdapter().getItem(getListView().getCheckedItemPosition()));

                return true;
            default:
                return false;

        }
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {

    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
        getListView().getCheckedItemCount();
        actionMode.setSubtitle(String.valueOf(  getListView().getCheckedItemCount()));
        actionMode.invalidate();
        ((ExpensesListViewAdapter)getListAdapter()).notifyDataSetChanged();

    }


    public interface BudgetExpensesDialogsListener {

        public void showExpenseDialog(Expense expense);

        public void showEditExpenseDialog(Expense expense);

        public void showDeleteExpenseDialog(ArrayList<Expense> expenses);
    }

}
