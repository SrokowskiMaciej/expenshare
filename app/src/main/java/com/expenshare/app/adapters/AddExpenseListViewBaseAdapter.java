package com.expenshare.app.adapters;

import android.widget.BaseAdapter;

import com.expenshare.app.businesslogic.PersonExpense;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Maciek on 2014-06-10.
 */
public abstract class AddExpenseListViewBaseAdapter extends BaseAdapter{



    public abstract void add(PersonExpense object) ;

    public abstract void remove(PersonExpense object);
    public abstract ArrayList<PersonExpense> getPersonsExpensesList();

    public abstract void setTotalExpense(BigDecimal expense);
    public abstract BigDecimal getTotalExpense();

    public abstract void equalizeAllExpenses();


}
