package com.expenshare.app.businesslogic;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by Maciek on 2014-06-03.
 */
public class Utilities {

    public static BigDecimal getBigDecimalFromString(String string){
        String cleanString = string.toString().replaceAll("[.]", "");
        return new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR);
    }

    public static BigDecimal getBigDecimalFromIntX100(int valueX100){

        return new BigDecimal(valueX100).divide(new BigDecimal(100),2,BigDecimal.ROUND_HALF_UP);
    }

    public static int getIntX100FromBigDecimal(BigDecimal value){

        return value.movePointRight(2).intValue();
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy HH:mm:ss a");
        return sdf.format(c.getTime());
    }

    public static ArrayList<String> getAllAvailableCurrenciesCodes(){
        ArrayList<String> currencies = new ArrayList<String>();
        Locale[] locs = Locale.getAvailableLocales();

        for(Locale loc : locs) {
            try {
                String val= Currency.getInstance(loc).getCurrencyCode();
                if(!currencies.contains(val))
                    currencies.add(val);
            } catch(Exception exc)
            {
                // Locale not found
            }
            Collections.sort(currencies);
        }
        return currencies;
    }

}
