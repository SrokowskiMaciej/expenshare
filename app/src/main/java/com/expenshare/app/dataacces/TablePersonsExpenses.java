package com.expenshare.app.dataacces;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Maciek on 2014-05-02.
 */
public class TablePersonsExpenses {

    public static final String TABLE_PERSONS_EXPENSES = "persons_expenses";
    public static final String COLUMN_ID = "person_expense_id";
    public static final String COLUMN_PERSON_ID = "child_person_id";
    public static final String COLUMN_EXPENSE_ID = "child_expense_id";
    public static final String COLUMN_SPENDING_X_100 = "spending";
    public static final String COLUMN_PAYMENT_X_100 = "payment";
    public static final String COLUMN_SPENDING_LOCKED = "spendinglocked";
    public static final String COLUMN_PAYMENT_LOCKED = "paymentlocked";

    private static final String DATABASE_CREATE = "create table "
            + TABLE_PERSONS_EXPENSES
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_PERSON_ID + " integer not null, "
            + COLUMN_EXPENSE_ID + " integer not null, "
            + COLUMN_SPENDING_X_100 + " integer not null, "
            + COLUMN_PAYMENT_X_100 + " integer not null, "
            + COLUMN_SPENDING_LOCKED + " integer not null, "
            + COLUMN_PAYMENT_LOCKED + " integer not null, "
            +"FOREIGN KEY("+COLUMN_PERSON_ID+") REFERENCES "+TablePersons.TABLE_PERSONS+"("+TablePersons.COLUMN_ID+") ON DELETE CASCADE, "
            +"FOREIGN KEY("+COLUMN_EXPENSE_ID+") REFERENCES "+TableExpenses.TABLE_EXPENSES+"("+TableExpenses.COLUMN_ID+") ON DELETE CASCADE"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TablePersonsExpenses.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONS_EXPENSES);
        onCreate(database);
    }
}
