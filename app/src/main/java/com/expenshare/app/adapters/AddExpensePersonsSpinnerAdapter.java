package com.expenshare.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.expenshare.app.businesslogic.Person;

import java.util.ArrayList;


public class AddExpensePersonsSpinnerAdapter extends ArrayAdapter<Person> {

    private Context mContext;


    public AddExpensePersonsSpinnerAdapter(Context context, int resource, ArrayList<Person> objects) {
        super(context, resource, objects);
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Person person = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            holder.personName = (TextView) convertView.findViewById(android.R.id.text1);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }




        // Populate the data into the template view using the data object
        holder.personName.setText(person.getName());

        // Return the completed view to render on screen
        return convertView;
    }

    private class ViewHolder {
        public TextView personName;
    }
}