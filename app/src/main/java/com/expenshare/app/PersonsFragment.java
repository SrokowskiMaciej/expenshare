package com.expenshare.app;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;

import com.expenshare.app.adapters.PersonsAdapter;
import com.expenshare.app.businesslogic.Person;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dialogs.DialogFragmentShowPerson;
import com.expenshare.app.loaders.PersonsLoader;

import java.util.ArrayList;

public class PersonsFragment extends ListFragment implements   LoaderManager.LoaderCallbacks<ArrayList> {



    public PersonsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(543543, null, this);
    }



    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        DialogFragmentShowPerson dialog = DialogFragmentShowPerson.newInstance(((PersonsAdapter) getListAdapter()).getItem(position));
        dialog.show(getFragmentManager(), "deleteExpenseDialog");
    }




    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {
        return new PersonsLoader(getActivity(), MyContentProvider.CONTENT_URI_PERSONS, null, null, null, null);

    }
    @Override
    public void onLoadFinished(final Loader<ArrayList> loader, final ArrayList data) {

        getListView().post(new Runnable() {
            @Override
            public void run() {
                ArrayList<Person> mPersons = (ArrayList<Person>) data;
                setListAdapter(new PersonsAdapter(getActivity(),
                        R.layout.cell_list_view_persons,mPersons));
            }
        });

    }

    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }
}
