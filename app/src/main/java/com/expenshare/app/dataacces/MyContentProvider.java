package com.expenshare.app.dataacces;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class MyContentProvider extends ContentProvider {

    private MySQLiteOpenHelper database;

    private static final int BUDGETS = 10;
    private static final int BUDGET_ID = 20;
    private static final int PERSONS = 30;
    private static final int PERSON_ID = 40;
    private static final int EXPENSES = 50;
    private static final int EXPENSE_ID = 60;
    private static final int PERSONS_EXPENSES = 70;
    private static final int PERSON_EXPENSES_ID = 80;
    private static final int BUDGET_PERSONS = 100;
    private static final int BUDGET_TOTAL_EXPENSE = 110;
    private static final int BUDGET_PERSONS_COUNT = 120;
    private static final int EXPENSE_PERSONS_COUNT = 130;
    private static final int PERSON_BUDGET_SPENDING = 140;
    private static final int PERSON_BUDGET_PAYMENT = 150;
    private static final int BUDGET_CURRENCY = 160;

    private static final String AUTHORITY = "com.expenshare.app.contentprovider";

    private static final String BASE_PATH_BUDGETS = "persons_expenses/expenses/budgets";
    private static final String BASE_PATH_EXPENSES = "persons_expenses/expenses";
    private static final String BASE_PATH_PERSONS = "persons_expenses/persons";
    private static final String BASE_PATH_PERSONS_EXPENSES = "persons_expenses";
    private static final String BASE_PATH_BUDGET_PERSONS = "persons_expenses/expenses/budget_persons";
    private static final String BASE_PATH_BUDGET_TOTAL_EXPENSE = "expenses/budget_total_expense";
    private static final String BASE_PATH_BUDGET_PERSONS_COUNT = "budget_person_count";
    private static final String BASE_PATH_EXPENSE_PERSONS_COUNT = "expense_person_count";
    private static final String BASE_PATH_PERSON_BUDGET_SPENDING = "person_budget_payment";
    private static final String BASE_PATH_PERSON_BUDGET_PAYMENT = "person_budget_spending";
    private static final String BASE_PATH_BUDGET_CURRENCY = "budget_currency";

    public static final Uri CONTENT_URI_BUDGETS = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_BUDGETS);
    public static final Uri CONTENT_URI_EXPENSES = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_EXPENSES);
    public static final Uri CONTENT_URI_PERSONS = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_PERSONS);
    //Returns Join of persons and expenses
    public static final Uri CONTENT_URI_PERSONS_EXPENSES = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_PERSONS_EXPENSES);

    //Returns Join of persons and budgets
    public static final Uri CONTENT_URI_BUDGET_PERSONS = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_BUDGET_PERSONS);

    public static final Uri CONTENT_URI_BUDGET_TOTAL_EXPENSE = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_BUDGET_TOTAL_EXPENSE);

    public static final Uri CONTENT_URI_BUDGET_PERSONS_COUNT = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_BUDGET_PERSONS_COUNT);

    public static final Uri CONTENT_URI_EXPENSE_PERSONS_COUNT = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_EXPENSE_PERSONS_COUNT);

    public static final Uri CONTENT_URI_PERSON_BUDGET_SPENDING = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_PERSON_BUDGET_SPENDING);

    public static final Uri CONTENT_URI_PERSON_BUDGET_PAYMENT = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_PERSON_BUDGET_PAYMENT);

    public static final Uri CONTENT_URI__BUDGET_CURRENCY = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_BUDGET_CURRENCY);



    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_BUDGETS, BUDGETS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_BUDGETS + "/#", BUDGET_ID);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_EXPENSES, EXPENSES);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_EXPENSES + "/#", EXPENSE_ID);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_PERSONS, PERSONS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_PERSONS + "/#", PERSON_ID);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_PERSONS_EXPENSES, PERSONS_EXPENSES);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_PERSONS_EXPENSES + "/#", PERSON_EXPENSES_ID);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_BUDGET_PERSONS, BUDGET_PERSONS);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_BUDGET_TOTAL_EXPENSE + "/#", BUDGET_TOTAL_EXPENSE);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_BUDGET_PERSONS_COUNT + "/#", BUDGET_PERSONS_COUNT);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_EXPENSE_PERSONS_COUNT + "/#", EXPENSE_PERSONS_COUNT);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_BUDGET_CURRENCY+ "/#", BUDGET_CURRENCY);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_PERSON_BUDGET_SPENDING, PERSON_BUDGET_SPENDING);

        sURIMatcher.addURI(AUTHORITY, BASE_PATH_PERSON_BUDGET_PAYMENT, PERSON_BUDGET_PAYMENT);

    }


    public MyContentProvider() {
    }

    @Override
    public boolean onCreate() {
        database = new MySQLiteOpenHelper(getContext());
        return false;
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        long id;
        Uri uriOut;

        switch (uriType) {
            case BUDGETS:
                id = sqlDB.insert(TableBudgets.TABLE_BUDGETS, null, values);
                uriOut = Uri.withAppendedPath(CONTENT_URI_BUDGETS, String.valueOf(id));
                break;

            case PERSONS:
                id = sqlDB.insert(TablePersons.TABLE_PERSONS, null, values);
                uriOut = Uri.withAppendedPath(CONTENT_URI_PERSONS, String.valueOf(id));
                break;

            case EXPENSES:
                id = sqlDB.insert(TableExpenses.TABLE_EXPENSES, null, values);
                uriOut = Uri.withAppendedPath(CONTENT_URI_EXPENSES, String.valueOf(id));
                break;

            case PERSONS_EXPENSES:
                id = sqlDB.insert(TablePersonsExpenses.TABLE_PERSONS_EXPENSES, null, values);
                uriOut = Uri.withAppendedPath(CONTENT_URI_PERSONS_EXPENSES, String.valueOf(id));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return uriOut;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        String id;
        int rowsDeleted = 0;
        switch (uriType) {
            case BUDGETS:
                rowsDeleted = sqlDB.delete(TableBudgets.TABLE_BUDGETS, selection,
                        selectionArgs);
                break;
            case BUDGET_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(TableBudgets.TABLE_BUDGETS,
                            TableBudgets.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(TableBudgets.TABLE_BUDGETS,
                            TableBudgets.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                break;
            case PERSONS:
                rowsDeleted = sqlDB.delete(TablePersons.TABLE_PERSONS, selection,
                        selectionArgs);
                break;
            case PERSON_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(TablePersons.TABLE_PERSONS,
                            TablePersons.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(TablePersons.TABLE_PERSONS,
                            TablePersons.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                break;
            case EXPENSES:
                rowsDeleted = sqlDB.delete(TableExpenses.TABLE_EXPENSES, selection,
                        selectionArgs);
                break;
            case EXPENSE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(TableExpenses.TABLE_EXPENSES,
                            TableExpenses.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(TableExpenses.TABLE_EXPENSES,
                            TableExpenses.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                uri=CONTENT_URI_EXPENSES;
                break;
            case PERSONS_EXPENSES:
                rowsDeleted = sqlDB.delete(TablePersonsExpenses.TABLE_PERSONS_EXPENSES, selection,
                        selectionArgs);
                break;
            case PERSON_EXPENSES_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(TablePersonsExpenses.TABLE_PERSONS_EXPENSES,
                            TablePersonsExpenses.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(TablePersonsExpenses.TABLE_PERSONS_EXPENSES,
                            TablePersonsExpenses.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                uri=CONTENT_URI_PERSONS_EXPENSES;
                break;

            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        Cursor cursor;
        SQLiteDatabase db = database.getWritableDatabase();
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case BUDGETS:
                queryBuilder.setTables(TableBudgets.TABLE_BUDGETS);
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case BUDGET_ID:
                queryBuilder.setTables(TableBudgets.TABLE_BUDGETS);
                queryBuilder.appendWhere(TableBudgets.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case PERSONS:
                queryBuilder.setTables(TablePersons.TABLE_PERSONS);
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case PERSON_ID:
                queryBuilder.setTables(TablePersons.TABLE_PERSONS);
                queryBuilder.appendWhere(TablePersons.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case EXPENSES:
                queryBuilder.setTables(TableExpenses.TABLE_EXPENSES);
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case EXPENSE_ID:
                queryBuilder.setTables(TableExpenses.TABLE_EXPENSES);
                queryBuilder.appendWhere(TableExpenses.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case PERSONS_EXPENSES:
                queryBuilder.setTables(TablePersonsExpenses.TABLE_PERSONS_EXPENSES + " INNER JOIN " + TableExpenses.TABLE_EXPENSES + " ON " +
                        TablePersonsExpenses.COLUMN_EXPENSE_ID + " = " + TableExpenses.COLUMN_ID + " INNER JOIN " + TablePersons.TABLE_PERSONS + " ON " +
                        TablePersonsExpenses.COLUMN_PERSON_ID + " = " + TablePersons.COLUMN_ID);
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case PERSON_EXPENSES_ID:
                queryBuilder.setTables(TablePersonsExpenses.TABLE_PERSONS_EXPENSES + " INNER JOIN " + TableExpenses.TABLE_EXPENSES + " ON " +
                        TablePersonsExpenses.COLUMN_EXPENSE_ID + " = " + TableExpenses.COLUMN_ID + " INNER JOIN " + TablePersons.TABLE_PERSONS + " ON " +
                        TablePersonsExpenses.COLUMN_PERSON_ID + " = " + TablePersons.COLUMN_ID);
                queryBuilder.appendWhere(TablePersonsExpenses.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;

            case BUDGET_PERSONS:
                queryBuilder.setTables(TablePersons.TABLE_PERSONS + " INNER JOIN (SELECT DISTINCT " + TablePersonsExpenses.COLUMN_PERSON_ID + ", " + TableExpenses.COLUMN_BUDGET_ID + " FROM " +
                        TablePersonsExpenses.TABLE_PERSONS_EXPENSES + " INNER JOIN " + TableExpenses.TABLE_EXPENSES + " ON " + TablePersonsExpenses.COLUMN_EXPENSE_ID + " = " +
                        TableExpenses.COLUMN_ID + ") ON " + TablePersons.COLUMN_ID + " = " + TablePersonsExpenses.COLUMN_PERSON_ID + " INNER JOIN " +
                        TableBudgets.TABLE_BUDGETS + " ON " + TableExpenses.COLUMN_BUDGET_ID + " = " + TableBudgets.COLUMN_ID);

                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                /*cursor = db.rawQuery(
                        "SELECT * FROM " + TablePersons.TABLE_PERSONS + " INNER JOIN (SELECT DISTINCT " + TablePersonsExpenses.COLUMN_PERSON_ID + ", " + TableExpenses.COLUMN_BUDGET_ID + " FROM " +
                                TablePersonsExpenses.TABLE_PERSONS_EXPENSES + " INNER JOIN " + TableExpenses.TABLE_EXPENSES + " ON " + TablePersonsExpenses.COLUMN_EXPENSE_ID + " = " +
                                TableExpenses.COLUMN_ID + " WHERE " + TableExpenses.COLUMN_BUDGET_ID + "=?) ON " + TablePersons.COLUMN_ID + " = " + TablePersonsExpenses.COLUMN_PERSON_ID + " INNER JOIN " +
                                TableBudgets.TABLE_BUDGETS + " ON " + TableExpenses.COLUMN_BUDGET_ID + " = " + TableBudgets.COLUMN_ID, new String[]{uri.getLastPathSegment()}
                );*/
                break;
            case BUDGET_TOTAL_EXPENSE:

                cursor = db.rawQuery(
                        "SELECT SUM(" + TableExpenses.COLUMN_MONEY_X_100 + ") FROM " + TableExpenses.TABLE_EXPENSES + " WHERE " + TableExpenses.COLUMN_BUDGET_ID + "=?", new String[]{uri.getLastPathSegment()});

                break;

            case BUDGET_PERSONS_COUNT:
                cursor = db.rawQuery(
                        "SELECT COUNT(DISTINCT " + TablePersonsExpenses.COLUMN_PERSON_ID + ") FROM " + TablePersonsExpenses.TABLE_PERSONS_EXPENSES + " INNER JOIN " + TableExpenses.TABLE_EXPENSES + " ON " +
                                TablePersonsExpenses.COLUMN_EXPENSE_ID + " = " + TableExpenses.COLUMN_ID + " INNER JOIN " + TablePersons.TABLE_PERSONS + " ON " +
                                TablePersonsExpenses.COLUMN_PERSON_ID + " = " + TablePersons.COLUMN_ID + " WHERE " + TableExpenses.COLUMN_BUDGET_ID + "=?", new String[]{uri.getLastPathSegment()}
                );

                break;
            case EXPENSE_PERSONS_COUNT:

                cursor = db.rawQuery(
                        "SELECT COUNT(" + TablePersonsExpenses.COLUMN_PERSON_ID + ") FROM " + TablePersonsExpenses.TABLE_PERSONS_EXPENSES + " WHERE " + TablePersonsExpenses.COLUMN_EXPENSE_ID + "=?", new String[]{uri.getLastPathSegment()});

                break;

            case PERSON_BUDGET_SPENDING:


                cursor = db.rawQuery(
                        "SELECT SUM(" + TablePersonsExpenses.COLUMN_SPENDING_X_100 + ") FROM " + TablePersonsExpenses.TABLE_PERSONS_EXPENSES + " INNER JOIN " + TableExpenses.TABLE_EXPENSES + " ON " +
                                TablePersonsExpenses.COLUMN_EXPENSE_ID + " = " + TableExpenses.COLUMN_ID + " INNER JOIN " + TablePersons.TABLE_PERSONS + " ON " +
                                TablePersonsExpenses.COLUMN_PERSON_ID + " = " + TablePersons.COLUMN_ID + " WHERE " + selection, selectionArgs
                );

                break;
            case PERSON_BUDGET_PAYMENT:


                cursor = db.rawQuery(
                        "SELECT SUM(" + TablePersonsExpenses.COLUMN_PAYMENT_X_100 + ") FROM " + TablePersonsExpenses.TABLE_PERSONS_EXPENSES + " INNER JOIN " + TableExpenses.TABLE_EXPENSES + " ON " +
                                TablePersonsExpenses.COLUMN_EXPENSE_ID + " = " + TableExpenses.COLUMN_ID + " INNER JOIN " + TablePersons.TABLE_PERSONS + " ON " +
                                TablePersonsExpenses.COLUMN_PERSON_ID + " = " + TablePersons.COLUMN_ID + " WHERE " + selection, selectionArgs
                );
                break;

            case BUDGET_CURRENCY:
                queryBuilder.setTables(TableBudgets.TABLE_BUDGETS);
                queryBuilder.appendWhere(TableBudgets.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                cursor = queryBuilder.query(db, new String[]{TableBudgets.COLUMN_CURRENCY_CODE }, selection,
                        selectionArgs, null, null, sortOrder);

                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }


        return cursor;

    }


    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated;
        String id;
        switch (uriType) {
            case BUDGETS:
                rowsUpdated = sqlDB.update(TableBudgets.TABLE_BUDGETS, values, selection,
                        selectionArgs);
                break;
            case BUDGET_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(TableBudgets.TABLE_BUDGETS, values,
                            TableBudgets.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(TableBudgets.TABLE_BUDGETS, values,
                            TableBudgets.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                break;
            case PERSONS:
                rowsUpdated = sqlDB.update(TablePersons.TABLE_PERSONS, values, selection,
                        selectionArgs);
                break;
            case PERSON_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(TablePersons.TABLE_PERSONS, values,
                            TablePersons.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(TablePersons.TABLE_PERSONS, values,
                            TablePersons.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                break;
            case EXPENSES:
                rowsUpdated = sqlDB.update(TableExpenses.TABLE_EXPENSES, values, selection,
                        selectionArgs);
                break;
            case EXPENSE_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(TableExpenses.TABLE_EXPENSES, values,
                            TableExpenses.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(TableExpenses.TABLE_EXPENSES, values,
                            TableExpenses.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                uri=CONTENT_URI_EXPENSES;
                break;
            case PERSONS_EXPENSES:
                rowsUpdated = sqlDB.update(TablePersonsExpenses.TABLE_PERSONS_EXPENSES, values, selection,
                        selectionArgs);
                break;
            case PERSON_EXPENSES_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(TablePersonsExpenses.TABLE_PERSONS_EXPENSES, values,
                            TablePersonsExpenses.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(TablePersonsExpenses.TABLE_PERSONS_EXPENSES, values,
                            TablePersonsExpenses.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                uri=CONTENT_URI_PERSONS_EXPENSES;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

}
