package com.expenshare.app.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.expenshare.app.R;
import com.expenshare.app.businesslogic.PersonExpense;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;

public class AddExpenseVariesModeListViewAdapter extends AddExpenseListViewBaseAdapter {

    private Context mContext;
    private HashMap<ViewHolder, PersonExpense> mVisibleHoldersPersonsExpensesMap = new HashMap<ViewHolder, PersonExpense>();
    private ArrayList<PersonExpense> mPersonsExpenses;
    private BigDecimal totalExpense = new BigDecimal(0);
    private View.OnTouchListener externalOnTouchListener;


    public AddExpenseVariesModeListViewAdapter(Context context, ArrayList<PersonExpense> objects, BigDecimal totalExpense,  View.OnTouchListener externalOnTouchListener) {

        this.totalExpense=totalExpense;
        this.mContext = context;
        this.mPersonsExpenses = objects;
        this.externalOnTouchListener = externalOnTouchListener;
        equalizeUnlockedSpendings();
        equalizeUnlockedPayments();
    }


    public void add(PersonExpense object) {
        mPersonsExpenses.add(object);
        equalizeUnlockedSpendings();
        equalizeUnlockedPayments();
        notifyDataSetChanged();

    }

    public void remove(PersonExpense object) {
        //super.remove(object);
        mPersonsExpenses.remove(object);
        mVisibleHoldersPersonsExpensesMap.remove(object);
        checkForHavingAtLeastOneUnlockedPayment();
        checkForHavingAtLeastOneUnlockedSpending();
        equalizeUnlockedSpendings();
        equalizeUnlockedPayments();
        notifyDataSetChanged();
    }

    public ArrayList<PersonExpense> getPersonsExpensesList() {
        return mPersonsExpenses;
    }

    @Override
    public int getCount() {
        return mPersonsExpenses.size();
    }

    @Override
    public Object getItem(int i) {
        return mPersonsExpenses.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PersonExpense personExpense = mPersonsExpenses.get(position);
        ViewHolder holder;
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.cell_list_view_add_expense_varies_mode, parent, false);
            holder.personName = (TextView) convertView
                    .findViewById(R.id.personNameTextView);
            holder.personSpending = (EditText) convertView.findViewById(R.id.personSpendingEditText);
            holder.personSpendingCurrencyCodeTextView = (TextView) convertView
                    .findViewById(R.id.personSpendingCurrencyCodeTextView);
            holder.personPayment = (EditText) convertView
                    .findViewById(R.id.personPaymentEditText);
            holder.personPaymentCurrencyCodeTextView = (TextView) convertView
                    .findViewById(R.id.personPaymentCurrencyCodeTextView);
            holder.personPaymentLockButton = (ImageButton) convertView.findViewById(R.id.personPaymentLockImageButton);
            holder.deletePersonButton = (ImageButton) convertView.findViewById(R.id.deletePersonImageButton);
            holder.personSpendingLockButton = (ImageButton) convertView.findViewById(R.id.personSpendingLockImageButton);


            holder.personSpendingTextWatcher = new PersonSpendingTextWatcher(holder.personSpending, personExpense);
            holder.personSpending.addTextChangedListener(holder.personSpendingTextWatcher);
            setupEditTextFocusListener(holder.personSpending);
            setupExternalOnTouchListener(holder.personSpending);

            holder.personPaymentTextWatcher = new PersonPaymentTextWatcher(holder.personPayment, personExpense);
            holder.personPayment.addTextChangedListener(holder.personPaymentTextWatcher);
            setupEditTextFocusListener(holder.personPayment);
            setupExternalOnTouchListener(holder.personPayment);

            holder.lockSpendingButtonOnClickListener = new LockSpendingButtonOnClickListener(personExpense);
            holder.personSpendingLockButton.setOnClickListener(holder.lockSpendingButtonOnClickListener);
            setupExternalOnTouchListener(holder.personSpendingLockButton);

            holder.lockPaymentButtonOnClickListener = new LockPaymentButtonOnClickListener(personExpense);
            holder.personPaymentLockButton.setOnClickListener(holder.lockPaymentButtonOnClickListener);
            setupExternalOnTouchListener(holder.personPaymentLockButton);

            holder.deletePersonButtonOnClickListener = new DeleteButtonOnClickListener(personExpense);
            holder.deletePersonButton.setOnClickListener(holder.deletePersonButtonOnClickListener);
            setupExternalOnTouchListener(holder.deletePersonButton);


            convertView.setTag(holder);
            mVisibleHoldersPersonsExpensesMap.put(holder, personExpense);
        } else {
            holder = (ViewHolder) convertView.getTag();
            holder.personSpendingTextWatcher.setPersonExpense(personExpense);

            holder.personPaymentTextWatcher.setPersonExpense(personExpense);


            holder.lockSpendingButtonOnClickListener.setPersonExpense(personExpense);


            holder.lockPaymentButtonOnClickListener.setPersonExpense(personExpense);


            holder.deletePersonButtonOnClickListener.setPersonExpense(personExpense);
            mVisibleHoldersPersonsExpensesMap.put(holder, personExpense);
        }

        holder.personName.setText(personExpense.getPerson().getName());

        holder.personSpending.removeTextChangedListener(holder.personSpendingTextWatcher);
        holder.personSpending.setText(personExpense.getPersonSpending().toString());
        holder.personSpending.addTextChangedListener(holder.personSpendingTextWatcher);

        holder.personPayment.removeTextChangedListener(holder.personPaymentTextWatcher);
        holder.personPayment.setText(personExpense.getPersonPayment().toString());
        holder.personPayment.addTextChangedListener(holder.personPaymentTextWatcher);

        holder.personSpendingCurrencyCodeTextView.setText(Currency.getInstance(personExpense.getExpense().getCurrencyCode()).getSymbol());
        holder.personPaymentCurrencyCodeTextView.setText(Currency.getInstance(personExpense.getExpense().getCurrencyCode()).getSymbol());
        if (personExpense.isPersonSpendingLocked())
            holder.personSpendingLockButton.setImageResource(R.drawable.ic_lock_closed);
        else
            holder.personSpendingLockButton.setImageResource(R.drawable.ic_lock_open);
        if (personExpense.isPersonPaymentLocked())
            holder.personPaymentLockButton.setImageResource(R.drawable.ic_lock_closed);
        else
            holder.personPaymentLockButton.setImageResource(R.drawable.ic_lock_open);
        return convertView;
    }

    private int getLockedSpendingsCount() {
        int i = 0;
        for (PersonExpense personExpense : mPersonsExpenses) {
            if (personExpense.isPersonSpendingLocked())
                i++;
        }
        return i;

    }

    private int getLockedPaymentsCount() {
        int i = 0;
        for (PersonExpense personExpense : mPersonsExpenses) {
            if (personExpense.isPersonPaymentLocked())
                i++;
        }
        return i;

    }

    public void setTotalExpense(BigDecimal expense) {
        totalExpense = expense;
        checkIfTotalExpenseNotExceedSumOfLockedSpendings();
        checkIfTotalExpenseNotExceedSumOfLockedPayments();
        equalizeUnlockedSpendings();
        equalizeUnlockedPayments();
        notifyDataSetChanged();
    }
    public BigDecimal getTotalExpense(){
        return totalExpense;
    }
    public void equalizeAllExpenses(){
        for(PersonExpense personExpense:mPersonsExpenses){
            personExpense.setPersonPaymentLocked(false);
            personExpense.setPersonSpendingLocked(false);
        }
        equalizeUnlockedPayments();
        equalizeUnlockedSpendings();
        notifyDataSetChanged();
    }


    private void equalizeUnlockedSpendings() {
        int unlockedSpendingsCount = mPersonsExpenses.size() - getLockedSpendingsCount();
        if (unlockedSpendingsCount > 0) {
            BigDecimal totalUnlockedSpendings = totalExpense;
            for (PersonExpense personExpense : mPersonsExpenses) {
                if (personExpense.isPersonSpendingLocked())
                    totalUnlockedSpendings = totalUnlockedSpendings.subtract(personExpense.getPersonSpending());

            }


            BigDecimal unlockedPersonPayment=totalUnlockedSpendings.divide(new BigDecimal(unlockedSpendingsCount),2,BigDecimal.ROUND_CEILING);

            for (PersonExpense personExpense : mPersonsExpenses) {
                if (!personExpense.isPersonSpendingLocked())
                    personExpense.setPersonSpending(unlockedPersonPayment);

            }
        }

    }

    private void equalizeUnlockedPayments() {
        int unlockedPaymentsCount = mPersonsExpenses.size() - getLockedPaymentsCount();

        if (unlockedPaymentsCount > 0) {
            BigDecimal totalUnlockedPayments = totalExpense;
            for (PersonExpense personExpense : mPersonsExpenses)
                if (personExpense.isPersonPaymentLocked())
                    totalUnlockedPayments = totalUnlockedPayments.subtract(personExpense.getPersonPayment());


            BigDecimal unlockedPersonPayment=totalUnlockedPayments.divide(new BigDecimal(unlockedPaymentsCount),2,BigDecimal.ROUND_CEILING);

            for (PersonExpense personExpense : mPersonsExpenses) {
                if (!personExpense.isPersonPaymentLocked())
                    personExpense.setPersonPayment(unlockedPersonPayment);

            }


        }

    }

    private void checkIfTotalExpenseNotExceedSumOfLockedSpendings() {

        for (PersonExpense personExpense:mPersonsExpenses) {
            BigDecimal sumOfLockedSpendings = BigDecimal.ZERO;
            if(personExpense.isPersonSpendingLocked())
                sumOfLockedSpendings = sumOfLockedSpendings.add(personExpense.getPersonSpending());
            if (totalExpense.compareTo(sumOfLockedSpendings) == -1)
                personExpense.setPersonSpendingLocked(false);
        }
    }

    private void checkIfTotalExpenseNotExceedSumOfLockedPayments() {

        for (PersonExpense personExpense:mPersonsExpenses) {
            BigDecimal sumOfLockedPayments = BigDecimal.ZERO;
           if(personExpense.isPersonPaymentLocked())
                sumOfLockedPayments = sumOfLockedPayments.add(personExpense.getPersonPayment());
            if (totalExpense.compareTo(sumOfLockedPayments) == -1)
                personExpense.setPersonPaymentLocked(false);

        }
    }


    private void checkForHavingAtLeastOneUnlockedPayment() {

        if (mPersonsExpenses.size() > 0) {
            while (getLockedPaymentsCount()>= mPersonsExpenses.size()){
                for(int i=mPersonsExpenses.size()-1;i>=0;i--) {
                    if(mPersonsExpenses.get(i).isPersonPaymentLocked()){
                        mPersonsExpenses.get(i).setPersonPaymentLocked(false);
                        break;
                    }
                }

            }
        }
    }

    private void checkForHavingAtLeastOneUnlockedSpending() {
        if (mPersonsExpenses.size() > 0) {
            while (getLockedSpendingsCount()>= mPersonsExpenses.size()){
                for(int i=mPersonsExpenses.size()-1;i>=0;i--) {
                    if(mPersonsExpenses.get(i).isPersonSpendingLocked()){
                        mPersonsExpenses.get(i).setPersonSpendingLocked(false);
                        break;
                    }
                }

            }
        }
    }

    private void setupEditTextFocusListener(final EditText editText) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                editText.setSelection(editText.getText().length());
            }
        });
    }

    private void setupExternalOnTouchListener(final View view) {
        view.setOnTouchListener(externalOnTouchListener);
    }

    private boolean setPaymentLocked(PersonExpense personExpense, boolean lock) {
        if(lock){
            if (!personExpense.isPersonPaymentLocked() && getLockedPaymentsCount() >= mPersonsExpenses.size() - 1) {
                return false;
            }else {
                personExpense.setPersonPaymentLocked(true);
                return true;
            }
        }else{
            personExpense.setPersonPaymentLocked(false);
            return false;
        }

    }

    private boolean setSpendingLocked(PersonExpense personExpense, boolean lock) {
        if(lock){
            if (!personExpense.isPersonSpendingLocked() && getLockedSpendingsCount() >= mPersonsExpenses.size() - 1) {
                return false;
            }else {
                personExpense.setPersonSpendingLocked(true);
                return true;
            }
        }else{
            personExpense.setPersonSpendingLocked(false);
            return false;
        }
    }


    private BigDecimal checkIfNewSpendingDoesNotExceedTotalCost(BigDecimal newSpending, PersonExpense changedPersonsExpense) {

        BigDecimal sumOfLockedSpendingsExcludingChangedPerson = BigDecimal.ZERO;
        for (PersonExpense personExpense : mPersonsExpenses) {
            if (personExpense != changedPersonsExpense && personExpense.isPersonSpendingLocked())
                sumOfLockedSpendingsExcludingChangedPerson = sumOfLockedSpendingsExcludingChangedPerson.add(personExpense.getPersonSpending());
        }
        if (totalExpense.compareTo(sumOfLockedSpendingsExcludingChangedPerson.add(newSpending)) == -1) {
            Toast.makeText(mContext, R.string.dialog_fragment_add_expense_all_total_spendings_exceed_total_cost_warning_message, Toast.LENGTH_LONG).show();
            return totalExpense.subtract(sumOfLockedSpendingsExcludingChangedPerson);
        }
        return newSpending;
    }

    private BigDecimal checkIfNewPaymentDoesNotExceedTotalCost(BigDecimal newPayment, PersonExpense changedPersonsExpense) {

        BigDecimal sumOfLockedPaymentsExcludingChangedPerson = BigDecimal.ZERO;
        for (PersonExpense personExpense : mPersonsExpenses) {
            if (personExpense != changedPersonsExpense && personExpense.isPersonPaymentLocked())
                sumOfLockedPaymentsExcludingChangedPerson = sumOfLockedPaymentsExcludingChangedPerson.add(personExpense.getPersonPayment());
        }
        if (totalExpense.compareTo(sumOfLockedPaymentsExcludingChangedPerson.add(newPayment)) == -1) {
            Toast.makeText(mContext, R.string.dialog_fragment_add_expense_all_total_payments_exceed_total_cost_warning_message, Toast.LENGTH_LONG).show();
            return totalExpense.subtract(sumOfLockedPaymentsExcludingChangedPerson);
        }
        return newPayment;
    }

    private void setSpendingsWithoutNotifyingDataSetChanged() {
        for (ViewHolder viewHolder : mVisibleHoldersPersonsExpensesMap.keySet()) {
            viewHolder.personSpending.removeTextChangedListener(viewHolder.personSpendingTextWatcher);
            viewHolder.personSpending.setText(mVisibleHoldersPersonsExpensesMap.get(viewHolder).getPersonSpending().toString());
            viewHolder.personSpending.addTextChangedListener(viewHolder.personSpendingTextWatcher);
        }
    }

    private void setPaymentsWithoutNotifyingDataSetChanged() {
        for (ViewHolder viewHolder : mVisibleHoldersPersonsExpensesMap.keySet()) {
            viewHolder.personPayment.removeTextChangedListener(viewHolder.personPaymentTextWatcher);
            viewHolder.personPayment.setText(mVisibleHoldersPersonsExpensesMap.get(viewHolder).getPersonPayment().toString());
            viewHolder.personPayment.addTextChangedListener(viewHolder.personPaymentTextWatcher);
        }
    }

    private void setPaymentsLocksWithoutNotifyingDataSetChanged() {
        for (ViewHolder viewHolder : mVisibleHoldersPersonsExpensesMap.keySet()) {
            if (mVisibleHoldersPersonsExpensesMap.get(viewHolder).isPersonPaymentLocked())
                viewHolder.personPaymentLockButton.setImageResource(R.drawable.ic_lock_closed);
            else
                viewHolder.personPaymentLockButton.setImageResource(R.drawable.ic_lock_open);
        }
    }

    private void setSpendingsLocksWithoutNotifyingDataSetChanged() {
        for (ViewHolder viewHolder : mVisibleHoldersPersonsExpensesMap.keySet()) {
            if (mVisibleHoldersPersonsExpensesMap.get(viewHolder).isPersonSpendingLocked())
                viewHolder.personSpendingLockButton.setImageResource(R.drawable.ic_lock_closed);
            else
                viewHolder.personSpendingLockButton.setImageResource(R.drawable.ic_lock_open);
        }
    }


    private class ViewHolder {
        public TextView personName;
        public EditText personSpending;
        public EditText personPayment;
        public TextView personSpendingCurrencyCodeTextView;
        public TextView personPaymentCurrencyCodeTextView;
        public PersonSpendingTextWatcher personSpendingTextWatcher;
        public PersonPaymentTextWatcher personPaymentTextWatcher;
        public ImageButton personSpendingLockButton;
        public LockSpendingButtonOnClickListener lockSpendingButtonOnClickListener;
        public ImageButton personPaymentLockButton;
        public LockPaymentButtonOnClickListener lockPaymentButtonOnClickListener;
        public ImageButton deletePersonButton;
        public DeleteButtonOnClickListener deletePersonButtonOnClickListener;

    }


    private class PersonSpendingTextWatcher implements TextWatcher {
        private EditText mEditText;
        private PersonExpense mPersonExpense;

        private PersonSpendingTextWatcher(EditText mEditText, PersonExpense mPersonExpense) {
            this.mEditText = mEditText;
            this.mPersonExpense = mPersonExpense;
        }

        public void setPersonExpense(PersonExpense personExpense) {
            this.mPersonExpense = personExpense;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!editable.toString().equals(mPersonExpense.getPersonSpending().toString())) {

                if (!mPersonExpense.isPersonSpendingLocked()) {
                    if (!setSpendingLocked(mPersonExpense, true)) {
                        Toast.makeText(mContext, R.string.dialog_fragment_add_expense_all_other_spendings_locked_warning_message, Toast.LENGTH_LONG).show();
                        mEditText.removeTextChangedListener(this);
                        mEditText.setText(mPersonExpense.getPersonSpending().toString());
                        mEditText.addTextChangedListener(this);
                        return;
                    }

                    setSpendingsLocksWithoutNotifyingDataSetChanged();
                }


                String cleanString = editable.toString().replaceAll("[.]", "");

                BigDecimal parsed = new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR);
                parsed = checkIfNewSpendingDoesNotExceedTotalCost(parsed, mPersonExpense);

                mPersonExpense.setPersonSpending(parsed);
                equalizeUnlockedSpendings();
                setSpendingsWithoutNotifyingDataSetChanged();
                mEditText.setSelection(mPersonExpense.getPersonSpending().toString().length());


            }

        }
    }

    private class PersonPaymentTextWatcher implements TextWatcher {
        private EditText mEditText;
        private PersonExpense mPersonExpense;

        private PersonPaymentTextWatcher(EditText mEditText, PersonExpense mPersonExpense) {
            this.mEditText = mEditText;
            this.mPersonExpense = mPersonExpense;
        }

        public void setPersonExpense(PersonExpense personExpense) {
            this.mPersonExpense = personExpense;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!editable.toString().equals(mPersonExpense.getPersonPayment().toString())) {

                if (!mPersonExpense.isPersonPaymentLocked()) {
                    if (!setPaymentLocked(mPersonExpense, true)) {
                        Toast.makeText(mContext, R.string.dialog_fragment_add_expense_all_other_payments_locked_warning_message, Toast.LENGTH_LONG).show();
                        mEditText.removeTextChangedListener(this);
                        mEditText.setText(mPersonExpense.getPersonPayment().toString());
                        mEditText.addTextChangedListener(this);
                        return;
                    }

                    setPaymentsLocksWithoutNotifyingDataSetChanged();
                }


                String cleanString = editable.toString().replaceAll("[.]", "");

                BigDecimal parsed = new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR);
                parsed = checkIfNewPaymentDoesNotExceedTotalCost(parsed, mPersonExpense);

                mPersonExpense.setPersonPayment(parsed);
                equalizeUnlockedPayments();
                setPaymentsWithoutNotifyingDataSetChanged();
                mEditText.setSelection(mPersonExpense.getPersonPayment().toString().length());


            }
        }
    }


    private class DeleteButtonOnClickListener implements View.OnClickListener {

        private PersonExpense mPersonExpense;

        private DeleteButtonOnClickListener(PersonExpense mPersonExpense) {
            this.mPersonExpense = mPersonExpense;
        }

        public void setPersonExpense(PersonExpense personExpense) {
            this.mPersonExpense = personExpense;
        }

        @Override
        public void onClick(View view) {
            remove(mPersonExpense);
            notifyDataSetChanged();
        }
    }


    private class LockSpendingButtonOnClickListener implements View.OnClickListener {

        private PersonExpense mPersonExpense;

        private LockSpendingButtonOnClickListener(PersonExpense mPersonExpense) {
            this.mPersonExpense = mPersonExpense;
        }

        public void setPersonExpense(PersonExpense personExpense) {
            this.mPersonExpense = personExpense;
        }

        @Override
        public void onClick(View view) {
            if (mPersonExpense.isPersonSpendingLocked())
                setSpendingLocked(mPersonExpense, false);
            else {
                if (!setSpendingLocked(mPersonExpense, true)) {
                    Toast.makeText(mContext, R.string.dialog_fragment_add_expense_all_other_spendings_locked_warning_message, Toast.LENGTH_LONG).show();
                }
            }
            equalizeUnlockedSpendings();
            setSpendingsWithoutNotifyingDataSetChanged();
            setSpendingsLocksWithoutNotifyingDataSetChanged();
        }
    }

    private class LockPaymentButtonOnClickListener implements View.OnClickListener {

        private PersonExpense mPersonExpense;

        private LockPaymentButtonOnClickListener(PersonExpense mPersonExpense) {
            this.mPersonExpense = mPersonExpense;
        }

        public void setPersonExpense(PersonExpense personExpense) {
            this.mPersonExpense = personExpense;
        }

        @Override
        public void onClick(View view) {
            if (mPersonExpense.isPersonPaymentLocked())
                setPaymentLocked(mPersonExpense, false);
            else {
                if (!setPaymentLocked(mPersonExpense, true)) {
                    Toast.makeText(mContext, R.string.dialog_fragment_add_expense_all_other_payments_locked_warning_message, Toast.LENGTH_LONG).show();
                }
            }
            equalizeUnlockedPayments();
            setPaymentsWithoutNotifyingDataSetChanged();
            setPaymentsLocksWithoutNotifyingDataSetChanged();
        }
    }

}