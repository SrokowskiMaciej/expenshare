package com.expenshare.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.businesslogic.Person;
import com.expenshare.app.businesslogic.PersonExpense;

import java.util.ArrayList;
import java.util.Currency;

/**
 * Created by Maciek on 2014-06-29.
 */
public class PersonsAdapter extends ArrayAdapter<Person> {

    private int layoutResource;
    private Context mContext;

    public PersonsAdapter(Context context, int layoutResource, ArrayList<Person> objects) {
        super(context, layoutResource, objects);
        this.mContext = context;
        this.layoutResource=layoutResource;
    }


    @Override
    public long getItemId(int i) {
        return getItem(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Person person = getItem(position);
        ViewHolder holder;
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = LayoutInflater.from(mContext).inflate(layoutResource, parent, false);
            holder.personName = (TextView) convertView
                    .findViewById(R.id.personNameTextView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

            holder.personName.setText(person.getName());

        return convertView;
    }


    private class ViewHolder {
        public TextView personName;
    }


}
