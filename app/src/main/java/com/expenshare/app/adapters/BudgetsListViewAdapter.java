package com.expenshare.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.businesslogic.Budget;

import java.util.ArrayList;
import java.util.Currency;

public class BudgetsListViewAdapter extends ArrayAdapter<Budget> {

    private Context mContext;


    public BudgetsListViewAdapter(Context context, int resource, ArrayList<Budget> objects) {
        super(context, resource, objects);
        this.mContext = context;
    }

    public BudgetsListViewAdapter(Context context, int resource) {
        super(context, resource);
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        Budget budget = getItem(position);
        ViewHolder holder;

        // Lookup view for data population
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cell_list_view_budgets, parent, false);
            holder.budgetName = (TextView) convertView.findViewById(R.id.budgetNameTextView);
            holder.checkBox=(CheckBox) convertView.findViewById(R.id.checkBox);
            holder.budgetDescription = (TextView) convertView.findViewById(R.id.budgetDescriptionTextView);
            holder.budgetTotalExpense = (TextView) convertView.findViewById(R.id.totalExpensesTextView);
            holder.budgetPersonNumber = (TextView) convertView.findViewById(R.id.personsInBudgetTextView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        // Populate the data into the template view using the data object
        holder.budgetName.setText(budget.getName());
        holder.budgetDescription.setText(budget.getDescription());
        holder.budgetTotalExpense.setText(" " + budget.getTotalExpenses().toString() + " " + Currency.getInstance(budget.getCurrencyCode()).getSymbol());
        holder.budgetPersonNumber.setText(String.valueOf(budget.getNumberOfPersons()));
        if(((ListView) parent).getCheckedItemPositions().get(position))
            holder.budgetName.setTextColor(mContext.getResources().getColor(R.color.white));
        else
            holder.budgetName.setTextColor(mContext.getResources().getColor(R.color.primary));

        if(holder.checkBox!=null){
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ListView listView=(ListView) parent;
                    if(listView.getCheckedItemPositions().get(position))
                        listView.setItemChecked(position,false);
                    else
                        listView.setItemChecked(position,true);
                }
            });
            ListView listView=(ListView) parent;
            if(listView.getCheckedItemPositions().get(position))
                holder.checkBox.setChecked(true);
            else
                holder.checkBox.setChecked(false);
        }


        return convertView;
    }

    public class ViewHolder {
        public CheckBox checkBox;
        public TextView budgetName;
        public TextView budgetDescription;
        public TextView budgetTotalExpense;
        public TextView budgetPersonNumber;
    }

}
