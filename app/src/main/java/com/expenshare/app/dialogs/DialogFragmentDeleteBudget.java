package com.expenshare.app.dialogs;

import android.app.Dialog;
import android.content.ContentUris;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.businesslogic.Budget;
import com.expenshare.app.dataacces.MyContentProvider;

import java.util.ArrayList;


public class DialogFragmentDeleteBudget extends DialogFragment implements View.OnClickListener{
    private TextView cancelButton;
    private TextView okButton;
    private ArrayList<Budget> deletedBudget;

    private final static String ARG_BUDGETS="budgets";

    public DialogFragmentDeleteBudget() {
    }

    static public DialogFragmentDeleteBudget newInstance(ArrayList<Budget> budgets) {
        DialogFragmentDeleteBudget f = new DialogFragmentDeleteBudget();
        Bundle bundle=new Bundle();
        bundle.putParcelableArrayList(ARG_BUDGETS, budgets);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.okButton:
                for(Budget budget:deletedBudget)
                    getActivity().getContentResolver().delete(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI_BUDGETS, budget.getId()), null, null);
                dismiss();

                break;
            case R.id.cancelButton:
                dismiss();
                break;

        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.dialog_fragment_budget_delete,null);

        builder.setView(view);

        deletedBudget =getArguments().getParcelableArrayList(ARG_BUDGETS);

        cancelButton = (TextView) view.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);
        okButton = (TextView) view.findViewById(R.id.okButton);
        okButton.setOnClickListener(this);
        return builder.create();
    }

}