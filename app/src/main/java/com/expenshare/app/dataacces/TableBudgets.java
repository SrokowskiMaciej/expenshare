package com.expenshare.app.dataacces;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Maciek on 2014-05-02.
 */
public class TableBudgets {

    public static final String TABLE_BUDGETS = "budgets";
    public static final String COLUMN_ID = "budget_id";
    public static final String COLUMN_NAME = "budget_name";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_CURRENCY_CODE = "currency_code";

    private static final String DATABASE_CREATE = "create table "
            + TABLE_BUDGETS
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_DESCRIPTION + " text, "
            + COLUMN_CURRENCY_CODE + " text"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TableBudgets.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_BUDGETS);
        onCreate(database);
    }
}
