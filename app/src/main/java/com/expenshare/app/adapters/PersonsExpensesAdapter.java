package com.expenshare.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.businesslogic.PersonExpense;

import java.util.ArrayList;
import java.util.Currency;

public class PersonsExpensesAdapter extends ArrayAdapter<PersonExpense> {

    private int layoutResource;
    private Context mContext;

    public PersonsExpensesAdapter(Context context, int layoutResource, ArrayList<PersonExpense> objects) {
        super(context, R.layout.cell_list_view_expenses_person, objects);
        this.mContext = context;
        this.layoutResource=layoutResource;
    }


    @Override
    public long getItemId(int i) {
        return getItem(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PersonExpense personExpense = getItem(position);
        ViewHolder holder;
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = LayoutInflater.from(mContext).inflate(layoutResource, parent, false);
            holder.personName = (TextView) convertView
                    .findViewById(R.id.personNameTextView);
            holder.expenseName = (TextView) convertView
                    .findViewById(R.id.expenseNameTextView);
            holder.personSpending = (TextView) convertView.findViewById(R.id.personSpendingTextView);

            holder.personSpendingCurrencyCodeTextView = (TextView) convertView
                    .findViewById(R.id.personSpendingCurrencyCodeTextView);
            holder.personPayment = (TextView) convertView
                    .findViewById(R.id.personPaymentTextView);
            holder.personPaymentCurrencyCodeTextView = (TextView) convertView
                    .findViewById(R.id.personPaymentCurrencyCodeTextView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (holder.personName != null)
            holder.personName.setText(personExpense.getPerson().getName());
        if (holder.expenseName != null)
            holder.expenseName.setText(personExpense.getExpense().getName());
        if (holder.personSpending != null)
            holder.personSpending.setText(personExpense.getPersonSpending().toString());
        if (holder.personPayment != null)
            holder.personPayment.setText(personExpense.getPersonPayment().toString());
        if (holder.personSpendingCurrencyCodeTextView != null)
            holder.personSpendingCurrencyCodeTextView.setText(Currency.getInstance(personExpense.getExpense().getCurrencyCode()).getSymbol());
        if (holder.personPaymentCurrencyCodeTextView != null)
            holder.personPaymentCurrencyCodeTextView.setText(Currency.getInstance(personExpense.getExpense().getCurrencyCode()).getSymbol());

        return convertView;
    }


    private class ViewHolder {
        public TextView personName;
        public TextView expenseName;
        public TextView personSpending;
        public TextView personPayment;
        public TextView personSpendingCurrencyCodeTextView;
        public TextView personPaymentCurrencyCodeTextView;
    }


}