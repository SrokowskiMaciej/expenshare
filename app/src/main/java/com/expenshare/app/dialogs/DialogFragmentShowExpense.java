package com.expenshare.app.dialogs;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.adapters.PersonsExpensesAdapter;
import com.expenshare.app.businesslogic.Budget;
import com.expenshare.app.businesslogic.Expense;
import com.expenshare.app.businesslogic.PersonExpense;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dataacces.TableBudgets;
import com.expenshare.app.dataacces.TableExpenses;
import com.expenshare.app.loaders.BudgetsLoader;
import com.expenshare.app.loaders.PersonsExpensesLoader;

import java.util.ArrayList;
import java.util.Currency;

public class DialogFragmentShowExpense extends DialogFragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<ArrayList> {

    private Button mOkButton;
    private TextView mExpenseNameEditText;
    private TextView mExpenseValueEditText;
    private TextView mBudgetNameTextView;
    private TextView mBudgetTotalExpenseTextView;
    private TextView mBudgetTotalExpenseCurrencyTextView;
    private ListView mListView;

    private Expense mExpense;

    //Adapters
    private PersonsExpensesAdapter mListViewAdapter;

    //Data
    private static final String EXPENSE_ARG = "expense";
    private static final String LOADER_ARG_EXPENSE_ID = "expenseId";
    private static final String LOADER_ARG_BUDGET_ID = "budgetId";

    private static final int LOADER_ID_BUDGET = 523;
    private static final int LOADER_ID_PERSONS_EXPENSES = 54;

    public DialogFragmentShowExpense() {
    }

    static public DialogFragmentShowExpense newInstance(Expense expense) {
        DialogFragmentShowExpense f = new DialogFragmentShowExpense();
        Bundle bundle = new Bundle();
        bundle.putParcelable(EXPENSE_ARG, expense);
        f.setArguments(bundle);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mExpense = getArguments().getParcelable(EXPENSE_ARG);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_show_expense, null);

        setupControls(view);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        startPersonsExpensesLoader(mExpense);
        startBudgetsLoader(mExpense);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.okButton:

                dismiss();
                break;
        }

    }


    private void setupControls(View view) {
        mExpenseNameEditText = (TextView) view.findViewById(R.id.titleTextView);
        mExpenseNameEditText.setText(mExpense.getName());
        mExpenseValueEditText = (TextView) view.findViewById(R.id.expenseValueTextView);
        mExpenseValueEditText.setText(mExpense.getTotalExpense().toString() + Currency.getInstance(mExpense.getCurrencyCode()).getSymbol());
        mBudgetNameTextView = (TextView) view.findViewById(R.id.budgetNameTextView);


        mBudgetTotalExpenseTextView = (TextView) view.findViewById(R.id.budgetTotalExpenseTextView);


        mBudgetTotalExpenseCurrencyTextView = (TextView) view.findViewById(R.id.budgetTotalExpenseCurrencyTextView);
        mOkButton = (Button) view.findViewById(R.id.okButton);
        mOkButton.setOnClickListener(this);

        mListView = (ListView) view.findViewById(R.id.personsExpensesListView);

    }


    private void startPersonsExpensesLoader(Expense expense) {
        Bundle bundle = new Bundle();
        bundle.putString(LOADER_ARG_EXPENSE_ID, String.valueOf(expense.getId()));
        getLoaderManager().initLoader(LOADER_ID_PERSONS_EXPENSES, bundle, this);
    }

    private void startBudgetsLoader(Expense expense) {
        Bundle bundle = new Bundle();
        bundle.putString(LOADER_ARG_BUDGET_ID, String.valueOf(expense.getBudgetId()));
        getLoaderManager().initLoader(LOADER_ID_BUDGET, bundle, this);
    }


    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {

        switch (id) {
            case LOADER_ID_PERSONS_EXPENSES:
                return new PersonsExpensesLoader(getActivity(), MyContentProvider.CONTENT_URI_PERSONS_EXPENSES, null, TableExpenses.COLUMN_ID + "=?", new String[]{args.getString(LOADER_ARG_EXPENSE_ID)}, null);

            case LOADER_ID_BUDGET:
                return new BudgetsLoader(getActivity(), MyContentProvider.CONTENT_URI_BUDGETS, null, TableBudgets.COLUMN_ID + "=?", new String[]{args.getString(LOADER_ARG_BUDGET_ID)}, null);

            default:
                return  null;
        }


    }


    @Override
    public void onLoadFinished(final Loader<ArrayList> loader, final ArrayList data) {

        switch (loader.getId()) {
            case LOADER_ID_PERSONS_EXPENSES:
                mListView.post(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<PersonExpense> personExpenses = (ArrayList<PersonExpense>) data;
                        mListViewAdapter = new PersonsExpensesAdapter(getActivity(), R.layout.cell_list_view_expenses_person, personExpenses);
                        mListView.setAdapter(mListViewAdapter);

                    }
                });
                break;

            case LOADER_ID_BUDGET:
                ArrayList<Budget> budgets = (ArrayList<Budget>) data;
                final Budget budget=budgets.get(0);
                mBudgetNameTextView.post(new Runnable() {
                    @Override
                    public void run() {
                        mBudgetNameTextView.setText(budget.getName());
                    }
                });


                mBudgetTotalExpenseTextView.post(new Runnable() {
                    @Override
                    public void run() {
                        mBudgetTotalExpenseTextView.setText(budget.getTotalExpenses().toString());
                    }
                });

                mBudgetTotalExpenseCurrencyTextView.post(new Runnable() {
                    @Override
                    public void run() {

                        mBudgetTotalExpenseCurrencyTextView.setText(Currency.getInstance(budget.getCurrencyCode()).getSymbol());
                    }
                });

                break;
        }


    }

    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }
}



