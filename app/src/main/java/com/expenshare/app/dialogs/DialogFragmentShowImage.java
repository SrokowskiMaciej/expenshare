package com.expenshare.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.expenshare.app.R;

/**
 * Created by Maciek on 2014-07-02.
 */
public class DialogFragmentShowImage extends DialogFragment {

    //Data
    private static final String IMAGE_RESOURCE_ARG = "img_resource";

    private int mImageResource;
    private ImageView mImageView;

    public DialogFragmentShowImage() {
    }

    static public DialogFragmentShowImage newInstance(int imageResource) {
        DialogFragmentShowImage f = new DialogFragmentShowImage();
        Bundle bundle = new Bundle();
        bundle.putInt(IMAGE_RESOURCE_ARG, imageResource);
        f.setArguments(bundle);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageResource = getArguments().getInt(IMAGE_RESOURCE_ARG);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_show_image, null);

        setupControls(view);
        builder.setView(view);

        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }



    private void setupControls(View view) {
        mImageView = (ImageView) view.findViewById(R.id.imageView);
        mImageView.setImageResource(mImageResource);
        mImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dismiss();
                return false;
            }
        });
    }

}



