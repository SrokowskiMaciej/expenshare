package com.expenshare.app.loaders;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.expenshare.app.businesslogic.Budget;
import com.expenshare.app.dataacces.TableBudgets;

import java.util.ArrayList;

public class BudgetsLoader extends BaseLoader {



    public BudgetsLoader(Context context, Uri uri, String[] projection, String selection,
                         String[] selectionArgs, String sortOrder) {
        super(context, uri, projection, selection, selectionArgs, sortOrder);
    }

    @Override
    public ArrayList loadInBackground() {

        Cursor cursor=getNewCursor();

        ArrayList<Budget> budgets = new ArrayList<Budget>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    Budget budget = new Budget();
                    budget.setName(cursor.getString(cursor.getColumnIndex(TableBudgets.COLUMN_NAME)));
                    budget.setDescription(cursor.getString(cursor.getColumnIndex(TableBudgets.COLUMN_DESCRIPTION)));
                    budget.setId(cursor.getLong(cursor.getColumnIndex(TableBudgets.COLUMN_ID)));
                    budget.setCurrencyCode(cursor.getString(cursor.getColumnIndex(TableBudgets.COLUMN_CURRENCY_CODE)));
                    budget.setTotalExpenses(getBudgetTotalExpense(budget.getId()));
                    budget.setNumberOfPersons(getBudgetPersonCount(budget.getId()));
                    budgets.add(budget);
                }
                while (cursor.moveToNext());
            }
        }
        cursor.close();
        return budgets;
    }

}
