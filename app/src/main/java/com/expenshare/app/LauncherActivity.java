package com.expenshare.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


public class LauncherActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mButtonStartExpenShare;
    private Button mButtonStartTutorial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        setupControls();
    }

    private void setupControls() {
        mButtonStartExpenShare = (Button) findViewById(R.id.startExpenShareButton);
        mButtonStartExpenShare.setOnClickListener(this);
        mButtonStartTutorial = (Button) findViewById(R.id.startTutorialButton);
        mButtonStartTutorial.setOnClickListener(this);
    }

    private void startMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    private void startTutorialActivity() {
        Intent intent = new Intent(getApplicationContext(), TutorialActivity.class);
        startActivity(intent);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.startExpenShareButton:
                startMainActivity();
                finish();
                break;
            case R.id.startTutorialButton:
                startTutorialActivity();
                break;
            default:

        }
    }
}
