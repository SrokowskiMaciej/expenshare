package com.expenshare.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.businesslogic.PersonBudget;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by Maciek on 2014-05-11.
 */
public class BudgetPersonsListViewAdapter extends ArrayAdapter<PersonBudget> {

    String mCurrencyCode;

    public BudgetPersonsListViewAdapter(Context context, int resource, ArrayList<PersonBudget> objects, String currencyCode) {
        super(context, resource, objects);
        mCurrencyCode=currencyCode;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PersonBudget personBudget = getItem(position);
        ViewHolder holder;
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.cell_list_view_budget_persons, parent, false);
            holder.personName = (TextView) convertView.findViewById(R.id.personNameTextView);
            holder.personSpendingInBudget = (TextView) convertView.findViewById(R.id.personSpendingTextView);
            holder.personTotalInBudget = (TextView) convertView.findViewById(R.id.personTotalTextView);
            holder.personPaymentInBudget = (TextView) convertView.findViewById(R.id.personPaymentTextView);
            holder.personSpendingInBudgetCurrency = (TextView) convertView.findViewById(R.id.personSpendingCurrencyCodeTextView);
            holder.personPaymentInBudgetCurrency = (TextView) convertView.findViewById(R.id.personPaymentCurrencyCodeTextView);
            holder.personTotalInBudgetCurrency = (TextView) convertView.findViewById(R.id.personTotalCurrencyCodeTextView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.personName.setText(personBudget.getPerson().getName());
        holder.personSpendingInBudget.setText(personBudget.getPersonSpending().toString());
        holder.personPaymentInBudget.setText(personBudget.getPersonPayment().toString());

        holder.personTotalInBudget.setText(personBudget.getPersonPayment().toString());
        BigDecimal totalDifference=personBudget.getPersonPayment().subtract(personBudget.getPersonSpending());

        if(totalDifference.signum()==-1)
            holder.personTotalInBudget.setTextColor(getContext().getResources().getColor(R.color.red));
        else
            holder.personTotalInBudget.setTextColor(getContext().getResources().getColor(R.color.green));
        holder.personTotalInBudget.setText(totalDifference.toString());
        holder.personSpendingInBudgetCurrency.setText(Currency.getInstance(mCurrencyCode).getSymbol());

        holder.personPaymentInBudgetCurrency.setText(Currency.getInstance(mCurrencyCode).getSymbol());
        holder.personTotalInBudgetCurrency.setText(Currency.getInstance(mCurrencyCode).getSymbol());
        return convertView;
    }


    private class ViewHolder {
        public TextView personName;
        public TextView personSpendingInBudget;
        public TextView personPaymentInBudget;
        public TextView personTotalInBudget;
        public TextView personSpendingInBudgetCurrency;
        public TextView personPaymentInBudgetCurrency;
        public TextView personTotalInBudgetCurrency;
    }

}
