package com.expenshare.app.businesslogic;



import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

public class Budget implements Parcelable {

    private String name;
    private String description;
    private int numberOfPersons;
    private BigDecimal totalExpenses;
    private String currencyCode;
    private long id;

    public Budget() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public BigDecimal getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(BigDecimal totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    protected Budget(Parcel in) {
        name = in.readString();
        description = in.readString();
        numberOfPersons = in.readInt();
        totalExpenses = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        currencyCode = in.readString();
        id = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(numberOfPersons);
        dest.writeValue(totalExpenses);
        dest.writeString(currencyCode);
        dest.writeLong(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Budget> CREATOR = new Parcelable.Creator<Budget>() {
        @Override
        public Budget createFromParcel(Parcel in) {
            return new Budget(in);
        }

        @Override
        public Budget[] newArray(int size) {
            return new Budget[size];
        }
    };
}