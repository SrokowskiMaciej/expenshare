package com.expenshare.app.businesslogic;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;


public class PersonExpense implements Parcelable {

    private Person person;
    private Expense expense;
    private BigDecimal personSpending =BigDecimal.ZERO;
    private BigDecimal personPayment =BigDecimal.ZERO;
    private boolean personSpendingLocked=false;
    private boolean personPaymentLocked=false;
    private long id;

    public PersonExpense() {
    }


    public PersonExpense(PersonExpense personExpense) {
        this.person=new Person(personExpense.person);
        this.expense=new Expense(personExpense.expense);
        this.personSpending=personExpense.personSpending;
        this.personPayment=personExpense.personPayment;
        this.personSpendingLocked=personExpense.personSpendingLocked;
        this.personPaymentLocked=personExpense.personPaymentLocked;
        this.id=personExpense.id;
    }

    public BigDecimal getPersonSpending() {
        return personSpending;
    }

    public void setPersonSpending(BigDecimal expense) {
        this.personSpending = expense;
    }

    public BigDecimal getPersonPayment() {
        return personPayment;
    }

    public void setPersonPayment(BigDecimal personPayment) {
        this.personPayment = personPayment;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Expense getExpense() {
        return expense;
    }

    public void setExpense(Expense expense) {
        this.expense = expense;
    }


    public boolean isPersonSpendingLocked() {
        return personSpendingLocked;
    }

    public void setPersonSpendingLocked(boolean personSpendingLocked) {
        this.personSpendingLocked = personSpendingLocked;
    }

    public boolean isPersonPaymentLocked() {
        return personPaymentLocked;
    }

    public void setPersonPaymentLocked(boolean personPaymentLocked) {
        this.personPaymentLocked = personPaymentLocked;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonExpense)) return false;

        PersonExpense that = (PersonExpense) o;

        if (!expense.equals(that.expense)) return false;
        if (!person.equals(that.person)) return false;

        return true;
    }


    protected PersonExpense(Parcel in) {
        person = (Person) in.readValue(Person.class.getClassLoader());
        expense = (Expense) in.readValue(Expense.class.getClassLoader());
        personSpending = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        personPayment = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        personSpendingLocked = in.readByte() != 0x00;
        personPaymentLocked = in.readByte() != 0x00;
        id = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(person);
        dest.writeValue(expense);
        dest.writeValue(personSpending);
        dest.writeValue(personPayment);
        dest.writeByte((byte) (personSpendingLocked ? 0x01 : 0x00));
        dest.writeByte((byte) (personPaymentLocked ? 0x01 : 0x00));
        dest.writeLong(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PersonExpense> CREATOR = new Parcelable.Creator<PersonExpense>() {
        @Override
        public PersonExpense createFromParcel(Parcel in) {
            return new PersonExpense(in);
        }

        @Override
        public PersonExpense[] newArray(int size) {
            return new PersonExpense[size];
        }
    };
}