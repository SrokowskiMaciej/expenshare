package com.expenshare.app.loaders;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.AsyncTaskLoader;

import com.expenshare.app.businesslogic.Utilities;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dataacces.TableExpenses;
import com.expenshare.app.dataacces.TablePersons;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

public class BaseLoader extends AsyncTaskLoader<ArrayList> {

    private ArrayList mObjects;

    protected final ForceLoadContentObserver mObserver;
    protected Uri mUri;
    protected String[] mProjection;
    protected String mSelection;
    protected String[] mSelectionArgs;
    protected String mSortOrder;
    public BaseLoader(Context context, Uri uri, String[] projection, String selection,
                         String[] selectionArgs, String sortOrder){
        super(context);
        mObserver = new ForceLoadContentObserver();
        mUri = uri;
        mProjection = projection;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mSortOrder = sortOrder;
    }

    protected Cursor getNewCursor(){
        Cursor newCursor = getContext().getContentResolver().query(mUri, mProjection, mSelection,
                mSelectionArgs, mSortOrder);
        return newCursor;
    }

    @Override
    public ArrayList loadInBackground() {
        return null;
    }

    protected BigDecimal getPersonBudgetSpending(long budgetId, long personId){
        Cursor personBudgetSpendingCursor = getContext().getContentResolver().query(MyContentProvider.CONTENT_URI_PERSON_BUDGET_SPENDING, null, TableExpenses.COLUMN_BUDGET_ID+"=? AND "+ TablePersons.COLUMN_ID+"=?",
                new String[]{String.valueOf(budgetId), String.valueOf(personId)}, null);
        personBudgetSpendingCursor.registerContentObserver(mObserver);
        BigDecimal totalExpense=BigDecimal.ZERO;
        if(personBudgetSpendingCursor!=null && personBudgetSpendingCursor.moveToFirst())
            totalExpense= Utilities.getBigDecimalFromIntX100(personBudgetSpendingCursor.getInt(0));
        personBudgetSpendingCursor.close();
        return totalExpense;
    }

    protected BigDecimal getPersonBudgetPayment(long budgetId, long personId){
        Cursor personBudgetPaymentCursor = getContext().getContentResolver().query(MyContentProvider.CONTENT_URI_PERSON_BUDGET_PAYMENT, null, TableExpenses.COLUMN_BUDGET_ID+"=? AND "+ TablePersons.COLUMN_ID+"=?",
                new String[]{String.valueOf(budgetId), String.valueOf(personId)}, null);
        personBudgetPaymentCursor.registerContentObserver(mObserver);
        BigDecimal totalExpense=BigDecimal.ZERO;
        if(personBudgetPaymentCursor!=null && personBudgetPaymentCursor.moveToFirst())
            totalExpense=Utilities.getBigDecimalFromIntX100(personBudgetPaymentCursor.getInt(0));
        personBudgetPaymentCursor.close();
        return totalExpense;
    }
    protected BigDecimal getBudgetTotalExpense(long budgetId){
        Uri budgetTotalExpenseUri= ContentUris.withAppendedId(MyContentProvider.CONTENT_URI_BUDGET_TOTAL_EXPENSE, budgetId);
        Cursor budgetTotalExpenseCursor = getContext().getContentResolver().query(budgetTotalExpenseUri, null, null,
                null, null);
        budgetTotalExpenseCursor.registerContentObserver(mObserver);
        BigDecimal totalExpense=BigDecimal.ZERO;
        if(budgetTotalExpenseCursor!=null && budgetTotalExpenseCursor.moveToFirst())
            totalExpense=Utilities.getBigDecimalFromIntX100(budgetTotalExpenseCursor.getInt(0));
        budgetTotalExpenseCursor.close();
        return totalExpense;
    }

    protected String getBudgetCurrencyCode(long budgetId){
        Uri budgetCurrencyCodeUri= ContentUris.withAppendedId(MyContentProvider.CONTENT_URI__BUDGET_CURRENCY, budgetId);
        Cursor budgetCurrencyCodeCursor = getContext().getContentResolver().query(budgetCurrencyCodeUri, null, null,
                null, null);
        budgetCurrencyCodeCursor.registerContentObserver(mObserver);
        String currencyCode=new String();
        if(budgetCurrencyCodeCursor!=null && budgetCurrencyCodeCursor.moveToFirst())
            currencyCode=budgetCurrencyCodeCursor.getString(0);
        budgetCurrencyCodeCursor.close();
        return currencyCode;
    }

    protected int getBudgetPersonCount(long budgetId){
        Uri budgetPersonsCountUri= ContentUris.withAppendedId(MyContentProvider.CONTENT_URI_BUDGET_PERSONS_COUNT,budgetId);
        Cursor budgetPersonsCountCursor = getContext().getContentResolver().query(budgetPersonsCountUri, null, null,
                null, null);
        budgetPersonsCountCursor.registerContentObserver(mObserver);
        int budgetPersonCount=0;
        if(budgetPersonsCountCursor!=null && budgetPersonsCountCursor.moveToFirst())
            budgetPersonCount=budgetPersonsCountCursor.getInt(0);
        budgetPersonsCountCursor.close();
        return budgetPersonCount;
    }

    protected int getExpensePersonCount(long expenseId){
        Uri expensePersonsCountUri= ContentUris.withAppendedId(MyContentProvider.CONTENT_URI_EXPENSE_PERSONS_COUNT,expenseId);
        Cursor expensePersonsCountCursor = getContext().getContentResolver().query(expensePersonsCountUri, null, null,
                null, null);
        expensePersonsCountCursor.registerContentObserver(mObserver);
        int budgetPersonCount=0;
        if(expensePersonsCountCursor!=null && expensePersonsCountCursor.moveToFirst())
            budgetPersonCount=expensePersonsCountCursor.getInt(0);
        expensePersonsCountCursor.close();
        return budgetPersonCount;
    }


    @Override
    protected void onStartLoading() {
        if (mObjects != null)
            deliverResult(mObjects);
        getContext().getContentResolver().registerContentObserver(mUri,true,mObserver);
        if (takeContentChanged() || mObjects == null)
            forceLoad();
    }

    /* Runs on the UI thread */
    @Override
    public void deliverResult(ArrayList budgets) {
        if (isStarted()) {
            super.deliverResult(budgets);
        }
    }

    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    @Override
    public void onCanceled(ArrayList budgets) {
    }



    @Override
    protected void onReset() {
        super.onReset();

        // Ensure the loader is stopped
        onStopLoading();

        getContext().getContentResolver().unregisterContentObserver(mObserver);
        mObjects = null;
    }


    public Uri getUri() {
        return mUri;
    }

    public void setUri(Uri uri) {
        mUri = uri;
    }

    public String[] getProjection() {
        return mProjection;
    }

    public void setProjection(String[] projection) {
        mProjection = projection;
    }

    public String getSelection() {
        return mSelection;
    }

    public void setSelection(String selection) {
        mSelection = selection;
    }

    public String[] getSelectionArgs() {
        return mSelectionArgs;
    }

    public void setSelectionArgs(String[] selectionArgs) {
        mSelectionArgs = selectionArgs;
    }

    public String getSortOrder() {
        return mSortOrder;
    }

    public void setSortOrder(String sortOrder) {
        mSortOrder = sortOrder;
    }

    @Override
    public void dump(String prefix, FileDescriptor fd, PrintWriter writer, String[] args) {
        super.dump(prefix, fd, writer, args);
        writer.print(prefix);
        writer.print("mUri=");
        writer.println(mUri);
        writer.print(prefix);
        writer.print("mProjection=");
        writer.println(Arrays.toString(mProjection));
        writer.print(prefix);
        writer.print("mSelection=");
        writer.println(mSelection);
        writer.print(prefix);
        writer.print("mSelectionArgs=");
        writer.println(Arrays.toString(mSelectionArgs));
        writer.print(prefix);
        writer.print("mSortOrder=");
        writer.println(mSortOrder);
        writer.print(prefix);
        writer.print("mCursor=");
        writer.print(prefix);
        writer.print("mContentChanged=");
        // writer.println(mContentChanged);
    }
}
