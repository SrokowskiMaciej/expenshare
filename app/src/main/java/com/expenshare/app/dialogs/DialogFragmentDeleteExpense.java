package com.expenshare.app.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentUris;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.businesslogic.Expense;
import com.expenshare.app.dataacces.MyContentProvider;

import java.util.ArrayList;

public class DialogFragmentDeleteExpense extends DialogFragment implements View.OnClickListener{
    private TextView cancelButton;
    private TextView okButton;
    private ArrayList<Expense> expensesToDelete;

    private final static String ARG_EXPENSES="expenses";

    public DialogFragmentDeleteExpense() {
    }

    static public DialogFragmentDeleteExpense newInstance(ArrayList<Expense> expenses) {
        DialogFragmentDeleteExpense f = new DialogFragmentDeleteExpense();
        Bundle bundle=new Bundle();
        bundle.putParcelableArrayList(ARG_EXPENSES, expenses);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.okButton:
                for(Expense expense:expensesToDelete)
                    getActivity().getContentResolver().delete(ContentUris.withAppendedId(MyContentProvider.CONTENT_URI_EXPENSES, expense.getId()), null, null);
                dismiss();

                break;
            case R.id.cancelButton:
                dismiss();
                break;

        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.dialog_fragment_expense_delete,null);

        builder.setView(view);

        expensesToDelete =getArguments().getParcelableArrayList(ARG_EXPENSES);

        cancelButton = (TextView) view.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);
        okButton = (TextView) view.findViewById(R.id.okButton);
        okButton.setOnClickListener(this);
        return builder.create();
    }

}