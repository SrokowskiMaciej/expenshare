package com.expenshare.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.expenshare.app.R;
import com.expenshare.app.businesslogic.Expense;

import java.util.ArrayList;
import java.util.Currency;

/**
 * Created by Maciek on 2014-05-07.
 */
public class ExpensesListViewAdapter extends ArrayAdapter<Expense> {


    private int layoutResource;
    public ExpensesListViewAdapter(Context context, int resource, ArrayList<Expense> objects) {
        super(context, resource, objects);
        layoutResource=resource;
    }

    public ExpensesListViewAdapter(Context context, int resource) {
        super(context, resource);
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        Expense expense = getItem(position);
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(layoutResource, parent, false);
            holder.expenseName = (TextView) convertView.findViewById(R.id.expenseNameTextView);
            holder.expenseDate = (TextView) convertView.findViewById(R.id.expenseDateTextView);
            holder.totalExpense = (TextView) convertView.findViewById(R.id.expenseSumTextView);
            holder.expensePersonNumber = (TextView) convertView.findViewById(R.id.personsInExpenseTextView);
            holder.checkBox=(CheckBox) convertView.findViewById(R.id.checkBox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }




        // Populate the data into the template view using the data object
        holder.expenseName.setText(expense.getName());
        if(holder.checkBox!=null) {
            if (((ListView) parent).getCheckedItemPositions().get(position))
                holder.expenseName.setTextColor(getContext().getResources().getColor(R.color.white));
            else
                holder.expenseName.setTextColor(getContext().getResources().getColor(R.color.primary));
        }
        holder.expenseDate.setText(expense.getDate());
        holder.totalExpense.setText(" " + expense.getTotalExpense().toString() + " " + Currency.getInstance(expense.getCurrencyCode()).getSymbol());
        holder.expensePersonNumber.setText(String.valueOf(expense.getNumberOfPersons()));
        if(holder.checkBox!=null){
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ListView listView=(ListView) parent;
                    if(listView.getCheckedItemPositions().get(position))
                        listView.setItemChecked(position,false);
                    else
                        listView.setItemChecked(position,true);
                }
            });
            ListView listView=(ListView) parent;
            if(listView.getCheckedItemPositions().get(position))
                holder.checkBox.setChecked(true);
            else
                holder.checkBox.setChecked(false);
        }
        // Return the completed view to render on screen
        return convertView;
    }

    private class ViewHolder {
        public TextView expenseName;
        public TextView expenseDate;
        public TextView totalExpense;
        public TextView expensePersonNumber;
        public CheckBox checkBox;
    }
}