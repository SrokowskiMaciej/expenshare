package com.expenshare.app;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;


import com.expenshare.app.adapters.ExpensesListViewAdapter;
import com.expenshare.app.businesslogic.Expense;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dialogs.DialogFragmentShowExpense;
import com.expenshare.app.loaders.ExpensesLoader;

import java.util.ArrayList;

public class ExpensesFragment extends ListFragment implements LoaderManager.LoaderCallbacks<ArrayList> {


    public ExpensesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getListView().setItemsCanFocus(true);


    }


    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(5743, null, this);
    }


    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {

        return new ExpensesLoader(getActivity(), MyContentProvider.CONTENT_URI_EXPENSES, null, null, null, null);

    }

    @Override
    public void onLoadFinished(final Loader<ArrayList> loader, final ArrayList data) {
        getListView().post(new Runnable() {
            @Override
            public void run() {
                ArrayList<Expense> expenses = (ArrayList<Expense>) data;
                setListAdapter(new ExpensesListViewAdapter(getActivity(),
                        R.layout.cell_list_view_expnses, expenses));
            }
        });
    }


    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Expense expense=((ExpensesListViewAdapter) getListAdapter()).getItem(position);
        DialogFragmentShowExpense dialog= DialogFragmentShowExpense.newInstance(expense);
        dialog.show(getFragmentManager(), "showExpenseDialog");

    }


}
