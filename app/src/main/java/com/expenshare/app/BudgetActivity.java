package com.expenshare.app;

import android.app.ActionBar;

import android.app.FragmentTransaction;
import android.content.ContentUris;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.expenshare.app.businesslogic.Budget;
import com.expenshare.app.businesslogic.Expense;
import com.expenshare.app.businesslogic.PersonBudget;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dialogs.DialogFragmentAddExpense;
import com.expenshare.app.dialogs.DialogFragmentDeleteExpense;
import com.expenshare.app.dialogs.DialogFragmentEditExpense;
import com.expenshare.app.dialogs.DialogFragmentShowBudgetPerson;
import com.expenshare.app.dialogs.DialogFragmentShowExpense;
import com.expenshare.app.loaders.BudgetsLoader;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;


public class BudgetActivity extends AppCompatActivity implements BudgetExpensesFragment.BudgetExpensesDialogsListener, BudgetPersonsFragment.BudgetPersonsDialogsListener, LoaderManager.LoaderCallbacks<ArrayList> {

    private static final String BUDGET_PARAM = "budget";
    private static final String LOADER_ARG_BUDGET = "budgetActivity";
    private Budget mBudget;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private TextView mPersonNumberTextView;
    private TextView mBudgetTotalExpenseTextView;
    private TextView mBudgetTotalExpenseCurrencyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget);
        setupData();
        setupControls();
    }

    private void setupControls() {
        mPersonNumberTextView = (TextView) findViewById(R.id.personNumberTextView);
        mBudgetTotalExpenseTextView = (TextView) findViewById(R.id.budgetTotalExpenseTextView);
        mBudgetTotalExpenseCurrencyTextView= (TextView) findViewById(R.id.budgetTotalExpenseCurrencyTextView);
        // Set up the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(mBudget.getName());
        //actionBar.setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(mViewPager);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.budget_expenses_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.add_expense_item:
                showAddExpenseDialog();
                return true;
            case android.R.id.home:
                finish();
                return true;
            case R.id.tutorial_item:
                startTutorialActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void startTutorialActivity() {
        Intent intent = new Intent(getApplicationContext(), TutorialActivity.class);
        startActivity(intent);
    }


    private void setupData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mBudget = extras.getParcelable(BUDGET_PARAM);
        }

    }

    private void startLoader() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(LOADER_ARG_BUDGET, mBudget);
        getSupportLoaderManager().initLoader(0, bundle, this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        startLoader();
    }

    public void showAddExpenseDialog() {
        DialogFragmentAddExpense dialog = DialogFragmentAddExpense.newInstance(mBudget);
        dialog.show(getSupportFragmentManager(), "addExpenseDialog");
    }

    @Override
    public void showExpenseDialog(Expense expense) {
        DialogFragmentShowExpense dialog= DialogFragmentShowExpense.newInstance(expense);
        dialog.show(getSupportFragmentManager(), "showExpenseDialog");
    }

    @Override
    public void showEditExpenseDialog(Expense expense) {
        DialogFragmentEditExpense dialog = DialogFragmentEditExpense.newInstance(mBudget, expense);
        dialog.show(getSupportFragmentManager(), "editExpenseDialog");
    }

    @Override
    public void showDeleteExpenseDialog(ArrayList<Expense> expenses) {
        DialogFragmentDeleteExpense dialog = DialogFragmentDeleteExpense.newInstance(expenses);
        dialog.show(getFragmentManager(), "deleteExpenseDialog");
    }


    @Override
    public void showBudgetPersonDialog(PersonBudget personBudget) {
        DialogFragmentShowBudgetPerson dialog = DialogFragmentShowBudgetPerson.newInstance(personBudget);
        dialog.show(getSupportFragmentManager(), "showPersonDialog");
    }



    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {
        BudgetsLoader budgetsLoader;
        Uri uri = ContentUris.withAppendedId(MyContentProvider.CONTENT_URI_BUDGETS, ((Budget) args.getParcelable(LOADER_ARG_BUDGET)).getId());
        budgetsLoader = new BudgetsLoader(this, uri,  null, null, null, null);
        return budgetsLoader;
    }

    @Override
    public void onLoadFinished(Loader<ArrayList> loader, ArrayList data) {
        //mBudget = BusinessLogicTranslator.getBudgets(data).get(0);
        mBudget=((ArrayList<Budget>) data).get(0);
        mBudgetTotalExpenseTextView.post(new Runnable() {
            @Override
            public void run() {
               mBudgetTotalExpenseTextView.setText(mBudget.getTotalExpenses().toString());
            }
        });
        mPersonNumberTextView.post(new Runnable() {
            @Override
            public void run() {
                mPersonNumberTextView.setText(String.valueOf(mBudget.getNumberOfPersons()));
            }
        });
        mBudgetTotalExpenseCurrencyTextView.post(new Runnable() {
            @Override
            public void run() {
                mBudgetTotalExpenseCurrencyTextView.setText(Currency.getInstance(mBudget.getCurrencyCode()).getSymbol());
            }
        });


    }

    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return BudgetExpensesFragment.newInstance(mBudget);
                case 1:
                    return BudgetPersonsFragment.newInstance(mBudget);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section3).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
            }
            return null;
        }
    }


}
