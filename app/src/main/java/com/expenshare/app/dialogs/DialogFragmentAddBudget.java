package com.expenshare.app.dialogs;

import android.app.Dialog;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.expenshare.app.R;
import com.expenshare.app.businesslogic.Utilities;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dataacces.TableBudgets;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;

public class DialogFragmentAddBudget extends DialogFragment implements View.OnClickListener{
    private TextView cancelButton;
    private TextView okButton;
    private EditText budgetNameEditText;
    private EditText budgetDescriptionEditText;
    private Spinner currencySpinner;
    private ArrayAdapter<String> currencyAdapter;

    public DialogFragmentAddBudget() {
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.okButton:
                if (budgetNameEditText.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.dialog_fragment_add_budget_name_empty_warning), Toast.LENGTH_LONG).show();
                    return;
                } else {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(TableBudgets.COLUMN_NAME, budgetNameEditText.getText().toString());
                    contentValues.put(TableBudgets.COLUMN_DESCRIPTION, budgetDescriptionEditText.getText().toString());
                    contentValues.put(TableBudgets.COLUMN_CURRENCY_CODE, currencyAdapter.getItem(currencySpinner.getSelectedItemPosition()));
                    getActivity().getContentResolver().insert(MyContentProvider.CONTENT_URI_BUDGETS,contentValues);
                    dismiss();
                }
                break;
            case R.id.cancelButton:
                dismiss();
                break;

        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.dialog_fragment_budget_add,null);

        builder.setView(view);

        budgetNameEditText =(EditText) view.findViewById(R.id.budgetNameEditText);
        budgetDescriptionEditText =(EditText) view.findViewById(R.id.budgetDescriptionEditText);

        currencySpinner=(Spinner) view.findViewById(R.id.currencySpinner);
        ArrayList<String> currencyCodes= Utilities.getAllAvailableCurrenciesCodes();
        currencyAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,currencyCodes);
        currencySpinner.setAdapter(currencyAdapter);
        String localCurrencyCode= Currency.getInstance(Locale.getDefault()).getCurrencyCode();
        currencySpinner.setSelection(currencyCodes.indexOf(localCurrencyCode));

        cancelButton = (TextView) view.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(this);
        okButton = (TextView) view.findViewById(R.id.okButton);
        okButton.setOnClickListener(this);
        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        budgetNameEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        return super.onCreateView(inflater, container, savedInstanceState);
    }
}