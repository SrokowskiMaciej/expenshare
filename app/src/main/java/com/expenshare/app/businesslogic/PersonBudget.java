package com.expenshare.app.businesslogic;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

public class PersonBudget implements Parcelable {

    private Person person;
    private Budget budget;
    private BigDecimal personPayment = new BigDecimal(0);
    private BigDecimal personSpending = new BigDecimal(0);
    private long id;

    public PersonBudget() {
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public BigDecimal getPersonPayment() {
        return personPayment;
    }

    public void setPersonPayment(BigDecimal personPayment) {
        this.personPayment = personPayment;
    }

    public BigDecimal getPersonSpending() {
        return personSpending;
    }

    public void setPersonSpending(BigDecimal personSpending) {
        this.personSpending = personSpending;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    protected PersonBudget(Parcel in) {
        person = (Person) in.readValue(Person.class.getClassLoader());
        budget = (Budget) in.readValue(Budget.class.getClassLoader());
        personPayment = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        personSpending = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        id = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(person);
        dest.writeValue(budget);
        dest.writeValue(personPayment);
        dest.writeValue(personSpending);
        dest.writeLong(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PersonBudget> CREATOR = new Parcelable.Creator<PersonBudget>() {
        @Override
        public PersonBudget createFromParcel(Parcel in) {
            return new PersonBudget(in);
        }

        @Override
        public PersonBudget[] newArray(int size) {
            return new PersonBudget[size];
        }
    };
}