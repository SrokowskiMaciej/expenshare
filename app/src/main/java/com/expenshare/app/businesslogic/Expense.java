package com.expenshare.app.businesslogic;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

public class Expense implements Parcelable {

    private String name;
    private String date;
    private int numberOfPersons;
    private BigDecimal totalAmount;
    private String currencyCode;

    private long id;
    private long budgetId;


    public Expense() {
    }

    public Expense(Expense expense) {
        this.name=expense.name;
        this.date=expense.date;
        this. numberOfPersons=expense.numberOfPersons;
        this.totalAmount=expense.totalAmount;
        this.id=expense.id;
        this.budgetId=expense.budgetId;
        this.currencyCode=expense.currencyCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getTotalExpense() {
        return totalAmount;
    }

    public void setTotalExpense(BigDecimal money) {
        this.totalAmount = money;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(long budgetId) {
        this.budgetId = budgetId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Expense)) return false;

        Expense expense = (Expense) o;

        if (id != expense.id) return false;

        return true;
    }




    protected Expense(Parcel in) {
        name = in.readString();
        date = in.readString();
        numberOfPersons = in.readInt();
        totalAmount = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        currencyCode = in.readString();
        id = in.readLong();
        budgetId = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(date);
        dest.writeInt(numberOfPersons);
        dest.writeValue(totalAmount);
        dest.writeString(currencyCode);
        dest.writeLong(id);
        dest.writeLong(budgetId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Expense> CREATOR = new Parcelable.Creator<Expense>() {
        @Override
        public Expense createFromParcel(Parcel in) {
            return new Expense(in);
        }

        @Override
        public Expense[] newArray(int size) {
            return new Expense[size];
        }
    };
}