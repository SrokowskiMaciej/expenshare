package com.expenshare.app.dataacces;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Maciek on 2014-05-02.
 */
public class TableExpenses {

    public static final String TABLE_EXPENSES = "expenses";
    public static final String COLUMN_ID = "expense_id";
    public static final String COLUMN_BUDGET_ID = "parent_budget_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DATE = "description";
    public static final String COLUMN_MONEY_X_100 = "money";

    private static final String DATABASE_CREATE = "create table "
            + TABLE_EXPENSES
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_BUDGET_ID + " integer, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_DATE + " text, "
            + COLUMN_MONEY_X_100 + " integer, "
            +"FOREIGN KEY("+COLUMN_BUDGET_ID+") REFERENCES "+TableBudgets.TABLE_BUDGETS+"("+TableBudgets.COLUMN_ID+") ON DELETE CASCADE"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TableExpenses.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSES);
        onCreate(database);
    }
}
