package com.expenshare.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;


import com.expenshare.app.adapters.BudgetsListViewAdapter;
import com.expenshare.app.businesslogic.Budget;
import com.expenshare.app.dataacces.MyContentProvider;
import com.expenshare.app.dialogs.DialogFragmentAddBudget;
import com.expenshare.app.dialogs.DialogFragmentDeleteBudget;
import com.expenshare.app.dialogs.DialogFragmentEditBudget;
import com.expenshare.app.loaders.BudgetsLoader;

import java.util.ArrayList;


public class BudgetsFragment extends ListFragment implements LoaderManager.LoaderCallbacks<ArrayList>,
        AdapterView.OnItemClickListener, AbsListView.MultiChoiceModeListener {


    private OnFragmentBudgetsListInteractionListener mListener;
    // private ActionMode mActionMode;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BudgetsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //getListView().getChe
        getListView().setOnItemClickListener(this);

        getListView().setDivider(getResources().getDrawable(R.color.primary_dark));
        getListView().setDividerHeight(1);
        getListView().setItemsCanFocus(true);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.budgets_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.add_budget_item:

                addBudget();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentBudgetsListInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentBudgetsListInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /*



        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);

            //getListView().setItemChecked(position,false);
            if (null != mListener) {
                mListener.onBudgetChosen((Budget) getListAdapter().getItem(position));
            }
        }
    */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        if (null != mListener) {

            mListener.onBudgetChosen((Budget) getListAdapter().getItem(position));
        }

    }


    @Override
    public Loader<ArrayList> onCreateLoader(int id, Bundle args) {
        return new BudgetsLoader(getActivity(), MyContentProvider.CONTENT_URI_BUDGETS, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList> cursorLoader, final ArrayList budgets) {

        getListView().post(new Runnable() {
            @Override
            public void run() {
                // if (mActionMode != null)
                //     mActionMode.finish();

                setListAdapter(new BudgetsListViewAdapter(getActivity(),
                        R.layout.cell_list_view_budgets, budgets));
                getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
                getListView().setMultiChoiceModeListener(BudgetsFragment.this);
            }
        });
    }


    @Override
    public void onLoaderReset(Loader<ArrayList> loader) {

    }


    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.budgets_context_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        menu.clear();
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.budgets_context_menu, menu);
        if (getListView().getCheckedItemCount() > 1) {
            menu.removeItem(R.id.edit_budget_item);
        }
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.edit_budget_item:
                SparseBooleanArray array = getListView().getCheckedItemPositions();
                int position = array.keyAt(array.indexOfValue(true));
                editBudget((Budget) getListAdapter().getItem(position));

                return true;
            case R.id.delete_budget_item:
                SparseBooleanArray checkedItemsPositions = getListView().getCheckedItemPositions();
                ArrayList<Budget> checkedBudgets = new ArrayList<Budget>();
                for (int i = 0; i < getListView().getCount(); i++) {
                    if (checkedItemsPositions.get(i))
                        checkedBudgets.add((Budget) getListAdapter().getItem(i));
                }
                deleteBudget(checkedBudgets);

                return true;
            default:
                return false;

        }
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        // mActionMode = null;
        //getListView().setItemChecked(getListView().getCheckedItemPosition(), false);
        //getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
        getListView().getCheckedItemCount();
        actionMode.setSubtitle(String.valueOf(getListView().getCheckedItemCount()));
        actionMode.invalidate();
        ((BudgetsListViewAdapter) getListAdapter()).notifyDataSetChanged();


    }


    public interface OnFragmentBudgetsListInteractionListener {
        public void onBudgetChosen(Budget budget);
    }

    public void deleteBudget(ArrayList<Budget> budget) {
        DialogFragmentDeleteBudget dialogFragmentDeleteBudget = DialogFragmentDeleteBudget.newInstance(budget);
        dialogFragmentDeleteBudget.show(getFragmentManager(), "deleteBudgetDialog");
    }


    public void addBudget() {
        DialogFragmentAddBudget dialogFragmentAddBudget = new DialogFragmentAddBudget();
        dialogFragmentAddBudget.show(getFragmentManager(), "addBudgetDialog");
    }


    public void editBudget(Budget budget) {
        DialogFragmentEditBudget dialogFragmentEditBudget = DialogFragmentEditBudget.newInstance(budget);
        dialogFragmentEditBudget.show(getFragmentManager(), "editBudgetDialog");
    }


}
