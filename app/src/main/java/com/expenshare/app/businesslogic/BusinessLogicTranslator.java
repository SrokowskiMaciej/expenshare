package com.expenshare.app.businesslogic;

import java.util.ArrayList;

/**
 * Created by Maciek on 2014-05-02.
 */
public class BusinessLogicTranslator {



    public static ArrayList<Person> pullPersonsOutOfPersonsBudgets(ArrayList<PersonBudget> personsBudgets) {
        ArrayList<Person> persons = new ArrayList<Person>(personsBudgets.size());
        for (PersonBudget personBudget : personsBudgets)
            persons.add(personBudget.getPerson());
        return persons;
    }

    public static ArrayList<Budget> pullBudgetsOutOfPersonsBudgets(ArrayList<PersonBudget> personsBudgets) {
        ArrayList<Budget> budgets = new ArrayList<Budget>(personsBudgets.size());
        for (PersonBudget personBudget : personsBudgets)
            budgets.add(personBudget.getBudget());
        return budgets;
    }

    public static ArrayList<Person> pullPersonsOutOfPersonsExpenses(ArrayList<PersonExpense> personsExpenses) {
        ArrayList<Person> persons = new ArrayList<Person>(personsExpenses.size());
        for (PersonExpense personExpense : personsExpenses)
            persons.add(personExpense.getPerson());
        return persons;
    }

    public static ArrayList<Expense> pullExpensesOutOfPersonsBudgets(ArrayList<PersonExpense> personsExpenses) {
        ArrayList<Expense> expenses = new ArrayList<Expense>(personsExpenses.size());
        for (PersonExpense personExpense : personsExpenses)
            expenses.add(personExpense.getExpense());
        return expenses;
    }

    public static PersonExpense getPersonExpenseWithPerson(ArrayList<PersonExpense> personsExpenses, Person person){
        for(PersonExpense personExpense:personsExpenses){
            if(personExpense.getPerson().equals(person))
                return personExpense;
        }
        return null;
    }
}
