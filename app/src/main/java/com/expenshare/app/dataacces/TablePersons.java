package com.expenshare.app.dataacces;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Maciek on 2014-05-02.
 */
public class TablePersons {

    public static final String TABLE_PERSONS = "persons";
    public static final String COLUMN_ID = "person_id";
    public static final String COLUMN_NAME = "person_name";

    private static final String DATABASE_CREATE = "create table "
            + TABLE_PERSONS
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(TablePersons.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONS);
        onCreate(database);
    }
}
