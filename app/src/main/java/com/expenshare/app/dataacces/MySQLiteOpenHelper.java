package com.expenshare.app.dataacces;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Maciek on 2014-05-02.
 */
public class MySQLiteOpenHelper  extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "smartexpenses.db";
    private static final int DATABASE_VERSION = 1;

    public MySQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        TableBudgets.onCreate(sqLiteDatabase);
        TableExpenses.onCreate(sqLiteDatabase);
        TablePersons.onCreate(sqLiteDatabase);
        TablePersonsExpenses.onCreate(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        TableBudgets.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
        TableExpenses.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
        TablePersons.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
        TablePersonsExpenses.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }
}
